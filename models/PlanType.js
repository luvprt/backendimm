var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var PlanTypeSchema = new Schema({
   name: { type: String },
   stripeProductId: { type: String },
   userType : { type: Schema.Types.ObjectId,  ref: "UserType", default: null },
   isDeleted: { type: Boolean, default: false },
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('PlanType', PlanTypeSchema);