var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var BillingAndServiceLocationSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: "User", default: null },
    locationType: {
        type: String,
        enum: ['Service', 'Billing'],
    },
    billToAlias: { type: String, default:null },
    buildingAlias: { type: String, default:null },
    streetAddress: { type:String, default: null},
    apt: { type:String, default: null},       
    city: { type:String, default: null},
    state: { type:String, default: null},
    postalCode: { type:String, default: null},
    addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    deletedAt: { type: Date, default: null }, 
    createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('BillingAndServiceLocation', BillingAndServiceLocationSchema);