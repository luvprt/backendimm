var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var CmsSchema = new Schema({
   metaDescription: { type: String },
   metaKeywords: { type: String },
   pageType: {
      type: String,
      enum: ['Service', 'Website'],
      default:'Service'
   },
   slug: { type: String },
   pageTitle: { type: String },
   pageContent: { type:String },
   parentPage: { type: Schema.Types.ObjectId, ref: "Cms", default: null},
   isDeleted: { type: Boolean, default: false },
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});

module.exports = Mongoose.model('Cms', CmsSchema);