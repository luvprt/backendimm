var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var PlanHighLightSchema = new Schema({
   userTypeId: { type: Schema.Types.ObjectId, ref: "UserType", default: null  },
   planTypeId: { type: Schema.Types.ObjectId, ref: "PlanType", default: null  },
   planHighLights: { type: String },
   isDeleted:{ type: Boolean, default: null },
   isActive:{ type: Boolean, default: true },
   description: { type: String },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});

module.exports = Mongoose.model('PlanHighLight', PlanHighLightSchema);