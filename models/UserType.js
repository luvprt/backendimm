var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var UserType = new Schema({
   name: { type: String },
   isDeleted: { type: Boolean, default: false },
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('UserType', UserType);