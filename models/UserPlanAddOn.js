var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var UserPlanAddOnSchema = new Schema({
   user: { type: Schema.Types.ObjectId, ref: "User", default: null},
   addOn: { type: Schema.Types.ObjectId, ref: "AddOn", default: null},
   plan: { type: Schema.Types.ObjectId, ref: "PlanType", default: null},
   transactionId:{ type: String, default: null  },
   isActive: { type: Boolean, default: true },
   isDeleted: { type: Boolean, default: false },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now }
});

module.exports = Mongoose.model('UserPlanAddOn', UserPlanAddOnSchema);