var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var EmailLogSchema = new Schema({
   workOrder: { type: Schema.Types.ObjectId }, // ref. will be added workOrder schema
   time: { type: Date, default: null  },
   date: { type: Date, default: null  },
   sentTo: { type: Schema.Types.ObjectId, ref: "User", default: null} ,
   emailMessage: { type: String, default: null },
   status: { type: String, default: null },
   addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
   isDeleted: { type: Boolean, default: false }, 
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('EmailLog', EmailLogSchema);