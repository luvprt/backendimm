var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var AddOnSchema = new Schema({
    name: { type: String },
    description: { type: String },
    price: { type: Number },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    deletedAt: { type: Date, default: null },
    createdAt: { type: Date, default: Date.now },
});

module.exports = Mongoose.model('AddOn', AddOnSchema);