var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var FinancialDataSchema = new Schema({
   user: { type: Schema.Types.ObjectId, ref: "User", default: null},
   defaultPaymentMethod: { type: String, default: null },
   paymentTerms: {type: String, default: null },
   discount: { type: Number, default: null},
   laborChargeType: { type: String, default: null},
   addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
   isDeleted: { type: Boolean, default: false },
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('FinancialData', FinancialDataSchema);