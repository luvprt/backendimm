var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var SubscriptionPlanSchema = new Schema({
   userType: { type: Schema.Types.ObjectId, ref: "UserType", default: null },
   planType: { type: Schema.Types.ObjectId, ref: "PlanType", default: null },
   stripePlanId: { type: String, default: null },
   price: { type: Number },
   duration: { type: Schema.Types.ObjectId, ref: "DurationType", default: null },
   numberOfPersons: { type: Number },
   addOns: [{ type: Schema.Types.ObjectId, ref: "AddOn", default: null }],
   isUnlimited: { type: Boolean, default: false },
   isDeleted: { type: Boolean, default: false },
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});

SubscriptionPlanSchema.index({ planName: 'text' });

module.exports = Mongoose.model('SubscriptionPlan', SubscriptionPlanSchema);