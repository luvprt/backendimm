var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var EmployeeSchema = new Schema({
    firstName: { type: String, default: null  },
    lastName: { type: String , default: null },
    email: { type: String , default: null },
    primaryPhone: { type: Number , default: null },
    secondaryPhone: { type: Number , default: null },
    streetAddress1: { type: String , default: null },
    streetAddress2: { type: String , default: null },
    city: { type: String , default: null },
    state: { type: String , default: null },
    zipCode: { type: Number , default: null },
    emergencyContactPhone: { type: Number , default: null },
    emergencyContactName: { type: String , default: null },
    emergencyContactRelation: { type: String , default: null },
    dob: { type: Date, default: null },
    driversLisenceNumber: { type: Number, default: null },
    driversExpirationDate: { type: Date, default: null },
    nameToDisplayonDispatch: { type: String },
    gender: {
        type: String,
        enum: ['Male', 'Female'],
        default:'Male'
    },
    isFieldWorker: {
        type: Boolean,
        default:false
    },
    fieldWorkerType:{
        type:String,
        enum:['Inspector','Apprentice','technician'],
        default:null
    },
    colorCode: { type: String ,default:null},
    jobTitle: { type: String ,default:null},
    employeeType: { type: Schema.Types.ObjectId, default: null },
    experienceLevel: { type: Schema.Types.ObjectId, default: null },
    department: { type: Schema.Types.ObjectId, default: null },
    supervisors: [{ type: Schema.Types.ObjectId, default: null }],
    shortBio: { type: String ,default:null},
    profileImage: { type: String,default:null },
    addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    deletedAt: { type: Date, default: null },
    createdAt: { type: Date, default: Date.now },
});

module.exports = Mongoose.model('Employee', EmployeeSchema);