var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var UserPlan = new Schema({
   subscriptionPlan: { type: Schema.Types.ObjectId, ref: "SubscriptionPlan", default: null},
   user: { type: Schema.Types.ObjectId, ref: "User", default: null},
   addOns: { type:Array },
   stripeSubscriptionPlanId:{ type: String, default: null  },
   stripeCustomerId:{ type: String, default: null  },
   stripePlanId:{ type: String, default: null  },
   stripeProductId:{ type: String, default: null  },
   isActive: { type: Boolean, default: true },
   isDeleted: { type: Boolean, default: false },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});

module.exports = Mongoose.model('UserPlan', UserPlan);