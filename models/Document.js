var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var DocumentSchema = new Schema({
   user: { type: Schema.Types.ObjectId, ref: "User", default: null},
   mimeType: { type: String, default: null }, //e.h .jpg,pfp
   fileName: { type: String, default: null },
   documentType: { 
      type:String, 
      enum:["Img","Doc"] 
   }, // image , doc
   addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
   isDeleted: { type: Boolean, default: false }, 
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('Document', DocumentSchema);