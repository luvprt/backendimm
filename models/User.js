var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var UserSchema = new Schema({
    email: { type: String },
    password: { type: String },
    prefix: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    isDeleted: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    deletedAt: { type: Date, default: null },
    createdAt: { type: Date, default: Date.now },
    phone: { type: Number },
    address: { type: String },
    apt: { type: String },
    city: { type: String },
    state: { type: String },
    zipcode: { type: String },
    userTypeId:{type: Schema.Types.ObjectId, ref: "UserType", default: null},
    companyDetails:{
        companyName:{
            type: String,
            default: null
        },
        jobTitle:{
            type: String,
            default: null
        },
        department:{
            type: String,
            default: null
        }
    },
    contractorDetails:{ type: Object, default: null },
    resetPasswordToken: { type: String, default: null },
    addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null},
    additionalInfo:{
        privateNotes:{
           type: String,
           default: null
        },
        publicNotes:{
            type: String,
            default: null
        }
    }
});

module.exports = Mongoose.model('User', UserSchema);