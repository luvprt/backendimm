var Mongoose = require('mongoose'),
   Schema = Mongoose.Schema;

var CallLogSchema = new Schema({
   time: { type: Date, default: null  },
   date: { type: Date, default: null  },
   to: { type: Schema.Types.ObjectId, ref: "User", default: null},
   from: { type: Schema.Types.ObjectId, ref: "User", default: null},
   details: { type: String, default: null },
   status: { type: String, default: null },
   callDuration: { type: Number, default: null  }, // in minutes
   addedBy: { type: Schema.Types.ObjectId, ref: "User", default: null },
   isDeleted: { type: Boolean, default: false }, 
   isActive: { type: Boolean, default: true },
   deletedAt: { type: Date, default: null },
   createdAt: { type: Date, default: Date.now },
});


module.exports = Mongoose.model('CallLog', CallLogSchema);