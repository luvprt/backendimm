const bcrypt = require('bcryptjs');
const passport    = require('passport');
const passportJWT = require("passport-jwt");

const ExtractJWT = passportJWT.ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = passportJWT.Strategy;
const UserModel = require('./models/User');
const config = require('./utils/config'); 
const response = require('./utils/response');


passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, cb) {
        return UserModel.findOne({email}).populate('userTypeId').populate('addedBy')
            .then(user => {
                if (!user) {
                    return cb(null, false, {message: response.NOT_FOUND});
                }else{
                    bcrypt.compare(password, user.password, function(err, result) {
                        if (err) {
                            return cb(null, false, err);
                        }else{
                            if (result) {
                                return cb(null, user, {
                                    message: response.LOGIN_SUCCESS
                                });
                            }else{
                                return cb(null, false, {message: response.INCORRECT_PASSWORD});
                            }
                        }
                    })
                }
            })
            .catch(err => {
                return cb(err);
            });
    }
));

passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey   : config.secret
    },
    function (jwtPayload, cb) {
        return UserModel.findById(jwtPayload.id).populate('userTypeId').populate('addedBy')
            .then(user => {
                return cb(null, user);
            })
            .catch(err => {
                return cb(err);
            });
    }
));