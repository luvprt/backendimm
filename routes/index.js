var express = require('express');
var app = express();
var router = express.Router();

const passport    = require('passport');
var auth = require('./auth');
const Utils = require('../utils/response');
const middleware = require('../utils/middleware');
const usersRouter = require('./users');
const customerRouter = require('./customer');
const cmsRouter = require('./cms');
const subscriptionPlanRouter = require('./subscriptionPlan')
const addOnRouter = require('./addOn')
const planHighLightRouter = require('./planHighLight')
const billingAndServiceLocationRouter = require('./billingAndServiceLocation')
const employee = require('./employee')

/*
	*routes without authorization.
*/
router.use('/auth', auth)

/*
	*routes with authorization
*/
router.use('/users', passport.authenticate('jwt', {session: false}), usersRouter);
router.use('/cms', passport.authenticate('jwt', {session: false}), cmsRouter);
router.use('/subscriptionPlans', passport.authenticate('jwt', {session: false}), subscriptionPlanRouter)
router.use('/addOns', passport.authenticate('jwt', {session: false}), addOnRouter)
router.use('/planHighLight', passport.authenticate('jwt', {session: false}), planHighLightRouter)
router.use('/location', passport.authenticate('jwt', {session: false}), billingAndServiceLocationRouter)
router.use('/employee', passport.authenticate('jwt', {session: false}), employee)
router.use('/customer', passport.authenticate('jwt', {session: false}),middleware.PermissionMiddleware, customerRouter);


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
