var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const add_on = require('../controllers/addOn.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    *api to create add ons for subscription plans
*/

router.route('/addAddOn').post([
    check("name").isLength({ min: 3 }).trim()
    .withMessage("Name must be at least 3 chars long "),
    check("description").isLength({ min: 3 }).trim()
    .withMessage("Description must be at least 3 chars long "),
    check("price").exists().isNumeric().withMessage("Price is required and must be numeric"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        add_on.add_add_on(req, res)
    }
}).all(methodNotAllowed);

/*
    *api to delete add ons for subscription plans
*/

router.route('/deleteAddOn/:addOnId').delete([
    check('addOnId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid addOn id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        add_on.delete_add_on(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to get list of add ons for subscription plans
*/

router.route('/getAddOns').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        add_on.get_add_ons(req, res)
    }
}).all(methodNotAllowed);

/*
    *api to get list of add ons for subscription plans
*/
router.route('/getAllAddOnsAndStats').get(add_on.get_all_add_ons_and_stats).all(methodNotAllowed);

/*
    *api to edit add ons for subscription plans
*/

router.route('/editAddOn').put([
    check('addOnId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid addOn id"),
    check("name").isLength({ min: 3 }).trim()
    .withMessage("Name must be at least 3 chars long "),
    check("description").isLength({ min: 3 }).trim()
    .withMessage("Description must be at least 3 chars long "),
    check("price").exists().withMessage("Price  is required"),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        add_on.edit_add_on(req, res)
    }
}).all(methodNotAllowed)

//get addon by id 
router.route('/getAddOnById/:addOnId').get([
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        add_on.get_add_on_by_id(req, res)
    }
}).all(methodNotAllowed);



module.exports = router;