const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth.ctrl');
const { check, validationResult } = require("express-validator");
const response = require('../utils/response');
const validate = require('../utils/validations');
const fs = require('fs');
const Utils = require ('../utils/response');
const subscription_plan_controller = require('../controllers/subscritionPlan.ctrl');
const cms_controller = require('../controllers/cms.ctrl');



const methodNotAllowed = (req, res, next) => res.sendStatus(405);

router.route('/login').post([
    check("email").isEmail()
    .withMessage("Enter Valid email"),
    check("password").isLength({ min: 3 })
    .withMessage("Password must be at least 3 chars long "),
],
(req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        req.body.email = req.body.email.toLowerCase();
        authController.login(req, res)
    }
}).all(methodNotAllowed);

router.route('/forgotPassword').post([
    check('email').isEmail().withMessage("Enter Valid Email"),
], (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.forgot_password(req, res)
    }
}).all(methodNotAllowed);



//  signup

  router.route('/registration').post(
        validate.contractorRegisterValidate(), (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) { 
            return res.status(412).json(response.sendError(errors.array()));
        } else if (req.body.password !== req.body.confirmPassword) {
            return res.status(412).json(response.error(response.PASSWORD_NOT_MATCHED));
        } else {
            req.body.email= req.body.email.toLowerCase();
            authController.registration(req, res)
        }
}).all(methodNotAllowed);




/*
    *api to get list of all subscription plans with plan type and highlight
*/
router.route('/getSubscriptionPlanWithHighlight').get(authController.get_subscription_plan_with_highlight).all(methodNotAllowed);


/*
    *api to fetch subscription plan by user id and plan id 
*/

router.route('/getSubscriptionPlanWithHighlightById').post([
    check('userTypeId').exists().withMessage("User type id is required"),
    check('planTypeId').exists().withMessage("plan type id is required"),
    check('userTypeId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid user type id"),
    check('planTypeId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan type id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.get_plan_subscription_by_id(req, res)
    }
}).all(methodNotAllowed);



/*
    * api to update user plan
*/

router.route('/updateUserPlan').put([
    check("userId").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid user id"),    
    check("userTypeId").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid user type id"),    
    check("addOns").exists().withMessage("addOns are required"),
    check("subscriptionPlan").exists().withMessage("subscriptionPlan  is required")
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.updateUserPlan(req, res)
    }
}).all(methodNotAllowed)


// api to create membershipplan

router.route('/createMembership').post([
    check("token").exists()
    .withMessage("token is required"),   
    check("userId").exists()
    .withMessage("user id is required"), 
    check("stripePlanId").exists()
    .withMessage("Sripe plan id is required"), 
    check("subscriptionPlanId").exists()
    .withMessage("Plan id is required"), 
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.create_membership(req, res)
    }
}).all(methodNotAllowed);

// contact us email

router.route('/contactUs').post([
    check("email").exists()
    .withMessage("Email is required"), 
    check("name").exists()
    .withMessage("Name is required"),     
    check("comments").exists()
    .withMessage("Comments is required"),    
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.contactUs(req, res)
    }
}).all(methodNotAllowed);

//get page by title (frontend)
router.route('/getPageByTitle/:title').get([
    check('title').exists().withMessage("Title is required"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.get_page_by_title(req, res)
    }
}).all(methodNotAllowed);


//get page by title (frontend)
router.route('/checkEmailExists/:email').get([
    check('email').exists().withMessage("Email is required"),
    check('email').isEmail().withMessage("Invalid email address"),
    
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.check_email_exists(req, res)
    }
}).all(methodNotAllowed);

// get all cms page by page type
router.route('/getPagesByPageType/:pageType').get(authController.get_pages_by_page_type).all(methodNotAllowed);

//get page by slug 
router.route('/getPageBySlug/:slug').get([
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.get_page_by_slug(req, res)
    }
}).all(methodNotAllowed);


router.route('/clearbit').post([  
    check('email').exists().withMessage("Email is required"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        authController.clearbit(req, res)
    }
}).all(methodNotAllowed);



module.exports = router;