var express = require('express');
var router = express.Router();
const { check,validationResult } = require("express-validator");
const user_controller = require('../controllers/user.crtl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);
/* GET users listing. */
router.route('/getUserDetails').get(user_controller.get_user_details).all(methodNotAllowed);

router.route('/verifyToken').get(user_controller.verify_token).all(methodNotAllowed);

router.route('/resetPassword').post([
    check('password').isLength({ min: 3 }).trim()
    .withMessage("Password must be at least 3 chars long"),
    check('confirmPassword').isLength({ min: 3 }).trim()
    .withMessage("Password must be at least 3 chars long"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else if (req.body.password !== req.body.confirmPassword) {
        return res.status(412).json(response.error(response.PASSWORD_NOT_MATCHED));
    } else {
        user_controller.reset_password(req,res)
    }
}).all(methodNotAllowed);


module.exports = router;


