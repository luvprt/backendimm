var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const employee = require('../controllers/employee.ctrl');
const response = require('../utils/response');
var multer  = require('multer');
const Utils = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    * api to add employee personal data
*/

var fields = [
    { name: 'profileImage', maxCount: 1 },
  ];
var extension = ["jpeg", "jpg", "png","JPEG","PNG","JPG"]; // allowed extentions
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/profileImage/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now()+file.originalname);
    }
  });

var upload = multer({ storage: storage,fileFilter: function (req, file, cb) {
    const ext = file.originalname.substr(file.originalname.lastIndexOf('.') + 1);
    const checkExt = extension.includes(ext);
    if(!checkExt) {
        req.body.is_image =false;
        return cb(null, false)
    }
        cb(null, true)
    }
}); 

router.route('/addEmployeePersonal').post([upload.fields(fields),
    check("firstName").exists().withMessage("firstName is required"),
    check("lastName").exists().withMessage("lastName is required"),
    check("gender").exists().withMessage("gender is required"),
    check("dob").exists().withMessage("dob is required"),
    check("colorCode").exists().withMessage("colorCode is required"),
    check("driversLisenceNumber").exists().withMessage("driversLisenceNumber is required"),
    check("driversExpirationDate").exists().withMessage("driversExpirationDate is required"),
    check("nameToDisplayonDispatch").exists().withMessage("nameToDisplayonDispatch is required"),
    check("isFieldWorker").exists().withMessage("isFieldWorker is required"),
    check("fieldWorkerType").exists().withMessage("fieldWorkerType is required"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        if(req.files.profileImage!=undefined){
            if(ext=req.files.profileImage[0].filename.split('.').pop()){
                var checkExt = extension.includes(ext);
                if(!checkExt){
                    res.status(400).json(Utils.error(Utils.INVALID_IMAGE_FORMAT))
                } else{
                req.body.profileImage= req.files.profileImage[0].filename;
                }
            }
        } else{
            if(req.body.is_image!=undefined){
                res.status(400).json(Utils.error(Utils.INVALID_IMAGE_FORMAT))
            }
            req.body.profileImage='';
        }
        console.log(req.body.profileImage);
        employee.add_employee_personal(req, res)    }
}).all(methodNotAllowed);



/*
    * api to add employeee other data
*/

router.route('/addEmployeeOtherData').post([
    check("id").exists().withMessage("employee id is required")
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        employee.add_employee_other_data(req, res)
    }
}).all(methodNotAllowed);


/*
    *api delete employee by id
*/

router.route('/deleteEmployee/:employeeId').delete([
    check('employeeId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid location id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    
    } else {
        employee.delete_employee(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to get employee list
*/

router.route('/getEmployees').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        employee.get_employees(req, res)
     }
}).all(methodNotAllowed);

 

/*
    * api to edit Location
*/

router.route('/editEmployee').put([
    upload.fields(fields),
    check('employeeId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid employee id"),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        if(req.files.profileImage!=undefined){
            if(ext=req.files.profileImage[0].filename.split('.').pop()){
                var checkExt = extension.includes(ext);
                if(!checkExt){
                    res.status(400).json(Utils.error(Utils.INVALID_IMAGE_FORMAT))
                } else{
                req.body.profileImage= req.files.profileImage[0].filename;
                }
            } 
        }else{
            if(req.body.is_image!=undefined){
                res.status(400).json(Utils.error(Utils.INVALID_IMAGE_FORMAT))
            }
           // req.body.profileImage='';
        }
        employee.edit_employee(req, res)
    }
}).all(methodNotAllowed)


/*
    *api to fetch employee by id 
*/

router.route('/getEmployeeById/:employeeId').get([
    check('employeeId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid employee id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        employee.get_employee_by_id(req, res)
    }
}).all(methodNotAllowed);

module.exports = router;