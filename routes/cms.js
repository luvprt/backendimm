var express = require('express');
var router = express.Router();
const { check,validationResult } = require("express-validator");
const cms_controller = require('../controllers/cms.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    *add cms page api
*/
router.route('/addCmsPage').post([
    check('metaDescription').isLength({ min: 3 }).trim()
    .withMessage("Meta description must be at least 3 chars long"),
    check('metaKeywords').isLength({ min: 3 }).trim()
    .withMessage("Meta keyword must be at least 3 chars long"),
    check('pageTitle').isLength({ min: 3 }).trim()
    .withMessage("Page title must be at least 3 chars long"),
    check('pageContent').isLength({ min: 3 }).trim()
    .withMessage("Page content must be at least 3 chars long")
    //check('parentPage').optional().matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid page id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        cms_controller.add_cms_page(req,res)
    }
}).all(methodNotAllowed);


/*
    *get list of cms pages
*/

// router.route('/getCmsPages?').get(cms_controller.get_cms_pages).all(methodNotAllowed);

 router.route('/getAllCmsPages').get(cms_controller.get_all_cms_pages).all(methodNotAllowed);


router.route('/getCmsPages').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        cms_controller.get_cms_pages(req,res) 
    }
}).all(methodNotAllowed);

router.route('/getPageById/:pageId').get([
    check('pageId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        cms_controller.get_page_by_id(req, res)
    }
}).all(methodNotAllowed);



/*
    *edit cms page api
*/

router.route('/editCmsPage').put([
    check('metaDescription').isLength({ min: 3 }).trim()
    .withMessage("Meta description must be at least 3 chars long"),
    check('metaKeywords').isLength({ min: 3 }).trim()
    .withMessage("Meta keyword must be at least 3 chars long"),
    check('pageTitle').isLength({ min: 3 }).trim()
    .withMessage("Page title must be at least 3 chars long"),
    check('pageContent').isLength({ min: 3 }).trim()
    .withMessage("Page content must be at least 3 chars long"),
    check('pageId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid page id")
    //check('parentPage').optional().matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid page id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        cms_controller.edit_cms_page(req,res)
    }
}).all(methodNotAllowed);  

/*
    *delete cms page api
*/

router.route('/deleteCmsPage/:pageId').delete([
    check('pageId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid page id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        cms_controller.delete_cms_page(req,res)
    }
}).all(methodNotAllowed);


module.exports = router;