var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const billing_and_service_location = require('../controllers/billingAndServiceLocation.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    * api to add location
*/

router.route('/addLocation').post([
    check("user").exists().withMessage("User id is required"),
    check("apt").exists().withMessage("Apt is required"),
    check("city").exists().withMessage("City is required"),
    check("state").exists().withMessage("State is required"),
    check("postalCode").exists().withMessage("Postalcode is required"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.add_location(req, res)
    }
}).all(methodNotAllowed);



/*
    *api delete location by id
*/

router.route('/deleteLocation/:locationId').delete([
    check('locationId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid location id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.delete_location(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to get location list
*/

router.route('/getLocation').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.get_locations(req, res)
    }
}).all(methodNotAllowed);



/*
    * api to edit Location
*/

router.route('/editLocation').put([
    check('locationId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid location id"),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.edit_location(req, res)
    }
}).all(methodNotAllowed)


/*
    *api to fetch location by id 
*/

router.route('/getLocationById/:locationId').get([
    check('locationId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid location id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.get_location_by_id(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to fetch customers by location
*/

router.route('/getCustomersByLocation').post([
    check('location').exists().withMessage("Location is required"),
    check('locationType').exists().withMessage("location type is required")
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.get_customers_by_location(req, res)
    }
}).all(methodNotAllowed);


/*
    * api to save customer location
*/

router.route('/saveCustomerLocations').post(
    [check('addresses').exists().withMessage("Addresses is required")],
    (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.save_customer_locations(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to fetch customers 
*/

router.route('/getCustomersByRole').get([], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        billing_and_service_location.get_customers_by_role(req, res)
    }
}).all(methodNotAllowed);


/*
    *api to change location customers 
*/

router.route('/changeLocationStatus').put(
    [check('status').exists().withMessage("Status is required"),
    check('locationId').exists().withMessage("LocationId is required")
], (req, res) => {
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
       return res.status(412).json(response.sendError(errors.array()));
   } else {
       billing_and_service_location.change_status(req, res)
   }
}).all(methodNotAllowed);




module.exports = router;