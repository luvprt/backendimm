var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const subscription_plan_controller = require('../controllers/subscritionPlan.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    *api to add subscription plan
*/

router.route('/addPlan').post([
    check("userType").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid user type id"),    
    check("planType").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan type id"),
    check("price").exists().withMessage("Price  is required"),
    check("duration").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid duration id"),
    check("isUnlimited").isBoolean().withMessage("is unlimited must be boolean value")
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.add_plan(req, res)
    }
}).all(methodNotAllowed);


/*
    *api delete subscription plan by id
*/

router.route('/deletePlan/:planId').delete([
    check('planId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.delete_plan(req, res)
    }
}).all(methodNotAllowed);

/*
    *api to get subscription plans list
*/

router.route('/getPlans').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.get_plans(req, res)
    }
}).all(methodNotAllowed);

/*
    *api to get list of add ons for subscription plans
*/
router.route('/getAllPlans').get(subscription_plan_controller.get_all_plans).all(methodNotAllowed);


/*
    *api to fetch subscription plan by id 
*/

router.route('/getPlanById/:planId').get([
    check('planId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.get_plan_by_id(req, res)
    }
}).all(methodNotAllowed);


/*
    * api to edit subscription plan details
*/

router.route('/editPlan').put([
    check('planId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan id"),
    check("userType").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid user type id"),    
    check("planType").matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid plan type id"),
    check("price").exists().withMessage("Price  is required"),
    check("duration").exists().withMessage("duration  is required"),
    check("isUnlimited").isBoolean().withMessage("is unlimited must be boolean value")
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.edit_plan(req, res)
    }
}).all(methodNotAllowed)

router.route('/addUsertype').post([
    check("name").isLength({ min: 3 }).trim()
    .withMessage("User Type must be at least 3 chars long "),    
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.add_user_type(req, res)
    }
}).all(methodNotAllowed);

router.route('/addPlanType').post([
    check("name").isLength({ min: 3 }).trim()
    .withMessage("Plan name must be at least 3 chars long "),    
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.add_plan_type(req, res)
    }
}).all(methodNotAllowed);

router.route('/addDurationType').post([
    check("name").isLength({ min: 3 }).trim()
    .withMessage("Plan name must be at least 3 chars long "),    
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        subscription_plan_controller.add_plan_duration(req, res)
    }
}).all(methodNotAllowed);

// router.route('/getUserAndPlanType').get(subscription_plan_controller.get_user_and_plan_type).all(methodNotAllowed);



module.exports = router;