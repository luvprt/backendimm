var express = require('express');
var router = express.Router();
const { check, validationResult, body } = require("express-validator");
const customerCtrl = require('../controllers/customer.ctrl');
var multer = require('multer');
const response = require('../utils/response');
var _ = require('lodash');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
* Api to create customer for subscription plans
*/

router.route('/addCustomer').post([
    check("prefix").isLength({ min: 1 }).trim()
        .withMessage("Prefix must be at least 1 char long."),
    check("firstName").isLength({ min: 3 }).trim()
        .withMessage("First Name must be at least 3 chars long."),
    check("lastName").isLength({ min: 3 }).trim()
        .withMessage("Last Name must be at least 3 chars long."),
    check("phone").isLength({ min: 10 }).isLength({ max: 15 }).isNumeric().trim()
        .withMessage("Phone must be between 10 to 15 digits."),
    check("email").isEmail().trim()
        .withMessage("Invalid email."),
    body("companyDetails").custom(val => {
        if (!_.has(val, 'companyName')) {
            throw new Error('Company Name is required.')
        } else if (_.has(val, 'companyName') && val.companyName.length < 3) {
            throw new Error('Company Name must be at least 3 chars long.')
        }
        return val;
    }),
    body("companyDetails").custom(val => {
        if (!_.has(val, 'jobTitle')) {
            throw new Error('Job title is required')
        } else if (_.has(val, 'jobTitle') && val.jobTitle.length < 3) {
            throw new Error('Job title must be at least 3 chars long.')
        }
        return val;
    }),
    body("companyDetails").custom(val => {
        if (!_.has(val, 'department')) {
            throw new Error('Department is required')
        } else if (_.has(val, 'department') && val.department.length < 3) {
            throw new Error('Department must be at least 3 chars long.')
        }
        return val;
    }),
    body("additionalInfo").custom(val => {
        if (!_.has(val, 'publicNotes')) {
            throw new Error('Public notes are required')
        } else if (_.has(val, 'publicNotes') && val.publicNotes.length < 3) {
            throw new Error('Public notes must be at least 3 chars long.')
        }
        return val;
    }),
    body("additionalInfo").custom(val => {
        if (!_.has(val, 'privateNotes')) {
            throw new Error('Private notes are required')
        } else if (_.has(val, 'privateNotes') && val.privateNotes.length < 3) {
            throw new Error('Private notes must be at least 3 chars long.')
        }
        return val;
    }),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        customerCtrl.addCustomer(req, res)
    }
}).all(methodNotAllowed);


/*
* Api to view for subscription plans
*/

router.route('/viewCustomer/:customerId').get([
    check('customerId').matches(/^[a-f\d]{24}$/i).withMessage("Enter customer id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        customerCtrl.getCustomerById(req, res)
    }
}).all(methodNotAllowed);


/*
* Api to save  Financial Data
*/

router.route('/saveFinancialData').post(
    [check('financialData').exists().withMessage("financialData is required"),
],
    (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        customerCtrl.save_financial_data(req, res)
    }
}).all(methodNotAllowed);



module.exports = router;