var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const plan_highlight_controller = require('../controllers/planHighLight.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);

/*
    * api to add highlight plan
*/

router.route('/addHighLight').post([
    check("userTypeId").exists().withMessage("User type id is required"),
    check("description").isLength({ min: 3 }).trim()
    .withMessage("Description must be at least 3 chars long "),
    check("planTypeId").exists().withMessage("Plan type id is required"),
    check("planHighLights").exists().withMessage("Plan Highlights is required")
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        plan_highlight_controller.add_highlight(req, res)
    }
}).all(methodNotAllowed);


/*
    *api delete highlight by id
*/

router.route('/deleteHighLight/:highlightId').delete([
    check('highlightId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid highlight id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        plan_highlight_controller.delete_highlight(req, res)
    }
}).all(methodNotAllowed);

/*
    *api to get subscription highlight plans list
*/

router.route('/getHighlight').post([
    check('offset').exists().withMessage('Please enter offset value'),
    check('limit').exists().withMessage('Please enter limit'),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        plan_highlight_controller.get_plans_highlight(req, res)
    }
}).all(methodNotAllowed);





/*
    *api to fetch subscription plan by id 
*/

router.route('/getPlanHighlightById/:highlightId').get([
    check('highlightId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid highlight id"),
], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        plan_highlight_controller.get_highlightplan_by_id(req, res)
    }
}).all(methodNotAllowed);


/*
    * api to edit highlight plan details
*/

router.route('/editHighlight').put([
    check('highlightId').matches(/^[a-f\d]{24}$/i).withMessage("Enter Valid highlight id"),
    check("userTypeId").isLength({ min: 3 }).trim()
    .withMessage("User Type must be at least 3 chars long "),
    check("description").isLength({ min: 3 }).trim()
    .withMessage("Description must be at least 3 chars long "),
    check("planTypeId").exists().withMessage("Plan type is required"),
], (req,res)=>{
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        plan_highlight_controller.edit_highlightplan(req, res)
    }
}).all(methodNotAllowed)


/*
    *api to get all user types and plan types
*/
router.route('/getAllUserPlanTypes').get(plan_highlight_controller.get_all_user_plan_types).all(methodNotAllowed);


module.exports = router;