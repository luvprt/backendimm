var express = require('express');
var router = express.Router();
const { check, validationResult } = require("express-validator");
const financial_data = require('../controllers/financialData.ctrl');
const response = require('../utils/response');

const methodNotAllowed = (req, res, next) => res.sendStatus(405);


/*
    * api to save financial data
*/

router.route('/saveFinancialData').post(
    [check('financialData').exists().withMessage("financialData is required"),
],
    (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(412).json(response.sendError(errors.array()));
    } else {
        financial_data.save_financial_data(req, res)
    }
}).all(methodNotAllowed);

module.exports = router;