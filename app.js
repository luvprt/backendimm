var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var indexRouter = require('./routes/index');
var cors = require('cors')
const url = process.env.MONGODB_URI;
const common = require('./common/common');
const config = require('./utils/config');

 const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./utils/swagger.json');

require('./passport');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');



app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));
app.set('superSecret', config.secret);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

try {
    mongoose.connect('mongodb://localhost/itm', {
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true
    })
} catch (error) {
    console.log(error, 'err');
}

// app.use(function(req, res, next) {
//     console.log(req.headers)
//     if (!req.headers['content-type']) {
//         return res.status(406).send({
//             success: false,
//             error: true,
//             message: 'content type not matched!'
//         });
//     } else {
//         next()
//     }
// });

common.insertAdmin(function (err, message) {
    if (err) {
        console.log('Error while inserting admin detail : ' + err)
    } else {
        console.log("admin detail inserted successfully");
    }
});

app.use(cors())

app.use('/api/v1',indexRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;