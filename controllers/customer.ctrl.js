const Utils = require('../utils/response');
const config = require('../utils/config');
const Customer = require('../models/User');
const async = require('async');
const FinancialData = require('../models/FinancialData');
const Document = require('../models/Document');
const UserType = require('../models/UserType');


/*
    *method to add add ons
*/

exports.addCustomer = function (req, res) {
        async.auto({
            checkCustomer: function (callback) {
                Customer.find({ isDeleted: false, email: req.body.email })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.EMAIL_ALREADY_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            getBusinessRepresentativeId: ['checkCustomer', (results, callback) => {
                UserType.findOne({ name:'Building Representative'},{name:1}).exec((err, response) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, response)
                    }
                })
            }],
            insertCustomer: ['getBusinessRepresentativeId', (results, callback) => {
                req.body.userTypeId = results.getBusinessRepresentativeId._id;
                req.body.addedBy = req.user._id;
                Customer(req.body).save((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, result)
                    }
                })
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(results.insertCustomer, Utils.ADD_CUSTOMER))
            }
        })

}


exports.getCustomerById = function (req, res) {
    Customer.findOne({ '_id': req.params.customerId,
     isDeleted: false, isActive: true }).populate('userTypeId')
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}


/*
   *method for save financial data
*/

exports.save_financial_data = async (req, res) => {
    finacialModel = req.body.financialData;
    try {
        const financial = await FinancialData.insertMany(finacialModel)
        res.status(200).json(Utils.success(financial, Utils.DATA_SAVED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}
