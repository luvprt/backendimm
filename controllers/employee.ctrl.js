const Utils = require('../utils/response');
const config = require('../utils/config');
const Employee = require('../models/Employee');
const async = require('async');
const mongoose = require('mongoose');
const fs = require('fs');
const process = require('process');



/*
   *method for add employee personal
*/
 
exports.add_employee_personal = async (req, res) => {
    employeeModel = req.body;
    employeeModel.addedBy = req.user.id;
    try {
        const employee = await Employee(employeeModel)
        var response = await employee.save();
        res.status(200).json(Utils.success(response, Utils.DATA_SAVED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}


/*
   *method for add employee other data
*/
 
exports.add_employee_other_data = async (req, res) => {
    employeeModel = req.body;
    employeeModel.addedBy = req.user.id;
    try {
        var response = await Employee.findOneAndUpdate({ _id: req.body.id }, { $set: req.body });
        res.status(200).json(Utils.success(response, Utils.DATA_SAVED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}

/*
    *method for delete empoyee
*/

exports.delete_employee= async (req, res) => {
        try {
            var response = await Employee.findOneAndUpdate(
                { _id: req.params.employeeId },
                { $set: { isDeleted: true, deletedAt: Date.now() } },
            );
            res.status(200).json(Utils.success(response, Utils.EMPLOYEE_DELETED))
        } catch (error) {
            res.status(400).json(Utils.error(response))
        }
}

/*
    *method for getting list of employee's
    *pagination
    *sorting
*/

exports.get_employees = async (req, res) => {
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const skip = limit * (offset - 1)
    let query = { isDeleted: false }
    let sortQuery = { createdAt: -1 }
    var match = { isDeleted: false }
    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }
    var mainQuery = [
            { $match: match },
        {
            $lookup:
            {
                from: "users",
                localField: "addedBy",
                foreignField: "_id",
                as: "addedBy"
            },
        },
        { $sort: sortQuery },
        { $skip: skip },
        { $limit: limit },
        {
            "$project": {
                "firstName":1 ,
                "lastname": 1,
                "primaryPhone": 1,
                "department": 1,
                "streetAddress1": 1,
                "city": 1,
                "jobTitle": 1,
                "state": 1,
                "zipCode": 1,
                "isDeleted": 1,
                "addedBy": {
                    $arrayElemAt: ["$addedBy", 0]
                },
                
            }
        }
    ];
   try {
        const employeesData = await Employee.aggregate(mainQuery);
        var count = await Employee.find(query).count();
            res.status(200).json(Utils.successWithCount(employeesData, Utils.DATA_FETCH, count))
    } catch (error) {
        res.status(400).json(Utils.error(error));
    }
}
/*
    *method to get employee by id
*/
exports.get_employee_by_id = function (req, res) {
    Employee.findOne({ _id: req.params.employeeId })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCHED))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

/*
    *method for editing employee
*/
     
exports.edit_employee = async (req, res) => {
    try {
        var response = await Employee.findOneAndUpdate({ _id: req.body.employeeId }, { $set: req.body });
       
        // delete file
        if(req.files.profileImage!=undefined && response.profileImage!=''){
            const filePath = process.cwd()+'/public/profileImage/'+response.profileImage; 
            fs.unlinkSync(filePath);
        }
        res.status(200).json(Utils.success(response, Utils.EMPLOYEE_UPDATED))
    } catch (error) {
        res.status(400).json(Utils.error(error));
    }
}

