const Utils = require('../utils/response');
const config = require('../utils/config');
const SubscriptionPlan = require('../models/SubscriptionPlan');
const UserType = require('../models/UserType');
const PlanType = require('../models/PlanType');
const DurationType = require('../models/DurationType')
const async = require('async');
var mongoose = require('mongoose');
const PlanHighLight = require('../models/PlanHighLight');
const stripe = require('../common/stripe');
const stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");



/*
    *method for add subscription plan
*/

exports.add_plan = function (req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkPlanName: function (callback) {
                if (req.body.isUnlimited) {
                    var check = {
                        isDeleted: false,
                        userType: req.body.userType,
                        planType: req.body.planType,
                        duration: req.body.duration,
                        isUnlimited: true
                    }
                }
                else {
                    var check = {
                        isDeleted: false,
                        userType: req.body.userType,
                        planType: req.body.planType,
                        price: req.body.price,
                        duration: req.body.duration,
                        numberOfPersons: req.body.numberOfPersons
                    }
                }

                SubscriptionPlan.find(check)
                    .exec((err, response) => {
                        console.log(err);
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.PLAN_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            fetchDurationtype: ['checkPlanName', (data, callback) => {
                var query = { _id: req.body.duration }
                DurationType.findOne(query).exec((err, result) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        callback(null, result)
                    }
                })
            }],
            fetchUserPlan: ['fetchDurationtype', (data, callback) => {
                var query = { _id: req.body.planType }
                PlanType.findOne(query).exec((err, result) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        response = {
                            duartion: data.fetchDurationtype,
                            planType: result
                        }
                        callback(null, response)
                    }
                })
            }],
            insertSubscriptionPlanStripe: ['fetchUserPlan', (results, callback) => {
                console.log(results.fetchUserPlan.planType);
                const request = {
                    amount: req.body.price * 100,
                    interval: results.fetchUserPlan.duartion.name == 'monthly' ? 'month' : 'year',
                    product: results.fetchUserPlan.planType.stripeProductId,
                    currency: "usd",
                    trial_period_days: 30,
                    nickname: results.fetchUserPlan.planType.name
                }
                stripeAPI.plans.create(request,
                    function (err, data) {
                        if (err) {
                            callback(err)
                        }
                        callback(null, data)
                    });
            },
            ],
            insertSubscriptionPlan: ['insertSubscriptionPlanStripe', (results, callback) => {
                if (req.body.isUnlimited) {
                    req.body.numberOfPersons = '';
                }
                req.body.stripePlanId = results.insertSubscriptionPlanStripe.id;
                SubscriptionPlan(req.body).save((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, Utils.PLAN_ADDED)
                    }
                });
            }]
        },

            (err, results) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    res.status(200).json(Utils.success(results.insertSubscriptionPlanStripe, results.insertSubscriptionPlan))
                }
            })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
};

/*
    *method for deleting subscription plan
*/

exports.delete_plan = function (req, res) {
    console.log(req)
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            deletePlanStripe: function (callback) {
                // stripe id is not comming
                if (req.body.stripePlanId == undefined || req.body.stripePlanId == null || req.body.stripePlanId == '') {
                    req.body.stripePlanId = null;
                }
                if (req.body.stripePlanId == null) {
                    data = {};
                    callback(null, data)
                } else {
                    stripeAPI.plans.del(
                        req.body.stripePlanId,
                        function (err, data) {
                            if (err) {
                                callback(err.raw.message)
                            } else {
                                callback(null, data)
                            }
                        }
                    );
                }
            },
            deletePlan: ['deletePlanStripe', (results, callback) => {
                if (req.body.isUnlimited) {
                    req.body.numberOfPersons = '';
                }
                SubscriptionPlan.findOneAndUpdate({ _id: req.params.planId }, { $set: { isDeleted: true, DeletedAt: Date.now() } }).exec((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        if (result) {
                            callback(null, result)
                        } else {
                            callback(Utils.PLAN_DOES_NOT_EXISTS)
                        }
                    }
                })
            }]
        }, (err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            }
            else if (response == null) {
                res.status(400).json(Utils.error(Utils.PLAN_DOES_NOT_EXISTS))
            } else {
                res.status(200).json(Utils.success(response.deletePlan, Utils.PLAN_DELETED))
            }
        })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }



}

/*
    *method for getting list of plans
    *search plan by name
    *pagination
    *sorting
*/

exports.get_plans = function (req, res) {
    var query = { isDeleted: false, isActive: true }
    var match = { isDeleted: false, isActive: true }
    const limit =parseInt(req.body.limit);
    const offset =parseInt(req.body.offset);
    const skip = limit * (offset - 1)
    let sortQuery = { createdAt: -1 } 
    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }
    if (req.body.planType && req.body.planType != "") {
        query.planType = req.body.planType;
        match.planType = mongoose.Types.ObjectId(req.body.planType);

    }
    if (req.body.userType && req.body.userType != "") {
        query.userType = req.body.userType;
        match.userType = mongoose.Types.ObjectId(req.body.userType);
    }
    
    var hplanQuery = [
        { $match :match },
        { $lookup:
            {
               from: "plantypes",
               localField: "planType",
               foreignField: "_id",
               as: "planType"
            },
        },
        { $lookup:
            {
               from: "usertypes",
               localField: "userType",
               foreignField: "_id",
               as: "userType"
            },
        },
        { $lookup:
            {
               from: "durationtypes",
               localField: "duration",
               foreignField: "_id",
               as: "duration"
            },
        },
        {$sort:sortQuery},
        {$skip: skip },
        {$limit: limit},
        { "$project": { 
            "isUnlimited":1,
            "numberOfPersons":1,
            "price":1,
            "isDeleted":1,
            "isActive":1,
            "addOns":1,
            "userType":{ 
                    $arrayElemAt: [ "$userType", 0 ] 
            },
            "planType":{ 
                $arrayElemAt: [ "$planType", 0 ] 
            },
            "duration":{ 
                $arrayElemAt: [ "$duration", 0 ] 
            }
        }},
    ];


    async.auto({
        fetchUsertype: (callback) => {
            UserType.find({name: { $in: ['Contractor', 'Building Representative']}}).exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    callback(null, result)
                }
            })
        },
        fetchUserPlan: ['fetchUsertype', (data, callback) => {
            PlanType.find({}).populate('userType').exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    response = {
                        userTypes: data.fetchUsertype,
                        planType: result
                    }
                    callback(null, response)
                }
            })
        }],
        fetchPlans: ['fetchUserPlan', (data, callback) => {
            // const limit = parseInt(req.body.limit)
            // const offset = parseInt(req.body.offset)
            // const skip = limit * (offset - 1)
            //     //{"planData.name": -1}
            // let sortQuery = { createdAt: -1 }
            // if (req.body.sortingField && req.body.sortingOrder) {
            //     sortQuery = {
            //         [req.body.sortingField]: req.body.sortingOrder
            //     }
            // }
            // if (req.body.planType && req.body.planType != "") {
            //     query.planType = req.body.planType
            // }
            // if (req.body.userType && req.body.userType != "") {
            //     query.userType = req.body.userType
            // }
            // SubscriptionPlan.find(query)
            //     .populate({ path: 'userType', select: 'name' })
            //     .populate({ path: 'planType', select: 'name' })
            //     .populate({ path: 'duration', select: 'name' }).skip(skip).limit(limit).sort(sortQuery)
                SubscriptionPlan.aggregate(hplanQuery)
                .exec((err, result) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        SubscriptionPlan.countDocuments(query).exec((err, count) => {
                            if (err) {
                                res.status(400).json(Utils.error(err))
                            } else {
                                response = {
                                    userTypes: data.fetchUserPlan.userTypes,
                                    planTypes: data.fetchUserPlan.planType,
                                    getPlans: result
                                }
                                res.status(200).json(Utils.successWithCount(response, Utils.DATA_FETCH, count))
                            }
                        })
                    }
                })
        }]
    })

}

/*
    *method for getting list of plans
    *search plan by name
    *pagination
    *sorting
*/

exports.get_all_plans = function (req, res) {
    let query = { isDeleted: false, isActive: true }
    SubscriptionPlan.aggregate([{
        $group: {
            _id: { userType: '$userType' },
            data: { $push: '$$ROOT' }
        },
    }
    ]).exec((err, response) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else if (response.length > 0) {
            res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
        } else {
            res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
        }
    })
}

exports.get_plan_by_id = function (req, res) {
    SubscriptionPlan.findOne({ '_id': req.params.planId, isDeleted: false, isActive: true }).populate('addOns')
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

/*
    *method for editing subscription plan
*/

exports.edit_plan = function (req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkPlanName: function (callback) {
                SubscriptionPlan.find({
                    isDeleted: false,
                    userType: req.body.userType,
                    planType: req.body.planType,
                    price: req.body.price,
                    duration: req.body.duration,
                    numberOfPersons: req.body.numberOfPersons,
                    _id: { $ne: req.body.planId }
                })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.PLAN_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            editSubscriptionPlanStripe: ['checkPlanName', (results, callback) => {
                // stripeAPI.plans.update(
                //     req.body.stripePlanId,
                //     {nickname: 'test'},
                //     function(err, data) {
                //         if(err){
                //             callback(err)
                //         } else{
                data = {};
                callback(null, data)
                //         }
                // });
            },
            ],
            editSubscriptionPlan: ['editSubscriptionPlanStripe', (results, callback) => {
                if (req.body.isUnlimited) {
                    req.body.numberOfPersons = '';
                }
                SubscriptionPlan.findOneAndUpdate({ _id: req.body.planId, isDeleted: false }, { $set: req.body }).exec((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        // if (result) {
                        callback(null, Utils.PLAN_UPDATED)
                        //} else {
                        //  callback(Utils.PLAN_DOES_NOT_EXISTS)
                        //}
                    }
                })
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.editSubscriptionPlan))
            }
        })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

exports.add_user_type = function (req, res) {
    UserType(req.body).save((err, response) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else {
            res.status(200).json(Utils.success(null, 'user type added'));
        }
    })
}

exports.add_plan_type = function (req, res) {
    PlanType(req.body).save((err, response) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else {
            res.status(200).json(Utils.success(null, 'user type added'));
        }
    })
}

exports.add_plan_duration = function (req, res) {
    DurationType(req.body).save((err, response) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else {
            res.status(200).json(Utils.success(null, 'user type added'));
        }
    })
}


// exports.get_userType_planType_durationType_addOns = function(req,res) {
//     const query = { isDeleted: false, isActive: true }
//     async.auto({
//         fetchUsertype:(callback)=>{
//             UserType.find(query).exec((err, result) => {
//                 if (err) {
//                     res.status(400).json(Utils.error(err))
//                 } else {
//                     callback(null, result)
//                 }
//             })    
//         },
//         fetchUserPlan:['fetchUsertype', (data,callback)=>{
//             UserPlan.find(query).exec((err, result) => {
//                 if (err) {
//                     res.status(400).json(Utils.error(err))
//                 } else {
//                     console.log(result)
//                     response = {
//                         userTypes: data.fetchUsertype,
//                         planType: result
//                     }
//                     callback(null, response)
//                 }
//             })
//         }],
//         fetchDuration: ['fetchUserPlan', (data, callback) => {
//             DurationType.find(query).exec((err, result) => {
//                 if (err) {
//                     res.status(400).json(Utils.error(err))
//                 } else {
//                     response = {
//                         userTypes: data.fetchUserPlan.userTypes,
//                         planType:  data.fetchUserPlan.planType,
//                         durations: result
//                     }
//                     res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
//                 }
//             })
//         }]
//     })
// }

