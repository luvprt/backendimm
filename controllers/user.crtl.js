const jwt = require('jsonwebtoken');
const passport = require('passport');
const Utils = require('../utils/response');
const config = require('../utils/config');
const bcrypt = require('bcryptjs');
const User = require('../models/User');
var clearbit = require('clearbit')('sk_5ecd344adc5e5fd45efc833d8deb0139');

exports.get_user_details = function(req, res) {
    res.send(req.user);
};

/*
    *verify password reset token method
*/
exports.verify_token = function(req,res){
    const reqToken = req.headers['authorization'].split(" ")[1]
    User.findOne({
        email: req.user.email,
        resetPasswordToken: reqToken,
        isDeleted: false,
        isActive: true
    })
    .exec((err, response) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else if (response) {
            res.status(200).json(Utils.success(null, Utils.VERIFIED))
        } else {
            res.status(401).json(Utils.authorisedError(Utils.EXPIRE_TOKEN_LINK))
        }
    })
}

/*
    *reset password method
*/
exports.reset_password = function(req, res) {
    bcrypt.hash(req.body.password, 10, function(err, hashpassword) {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else {
            req.body.password = hashpassword
            const reqToken = req.headers['authorization'].split(" ")[1];
            User.findOneAndUpdate({ '_id': req.user.id, isDeleted: false, isActive: true}, { $set: { password: req.body.password, resetPasswordToken: null } }, { new: true, upsert: false })
                .exec((err, response) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else if (response) {
                        const token = jwt.sign({ id: response._id }, config.secret);
                        res.status(200).json(Utils.successWithToken(response, Utils.PASSWORD_RESET, token))
                    } else {
                        res.status(401).json(Utils.authorisedError(Utils.NOT_FOUND))
                    }
                })
        }
    })
}
