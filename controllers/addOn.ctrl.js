const Utils = require('../utils/response');
const config = require('../utils/config');
const AddOn = require('../models/AddOn');
const PlanType = require ('../models/PlanType');
const UserType = require('../models/UserType');
const DurationType = require('../models/DurationType');
const async = require('async');

/*
    *method to add add ons
*/

exports.add_add_on = function (req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkName: function (callback) {
                AddOn.find({ isDeleted: false, name: req.body.name })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.ADD_ON_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            insertAddOn: ['checkName', (results, callback) => {
                AddOn(req.body).save((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, Utils.ADD_ON_ADDED)
                    }
                })
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.insertAddOn))
            }
        })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

/*
    *method to delete add on
*/

exports.delete_add_on = function (req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        AddOn.findOneAndUpdate({ _id: req.params.addOnId, isDeleted: false }, { $set: { isDeleted: true, deletedAT: Date.now() } })
            .exec((err, response) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else if (response == null) {
                    res.status(400).json(Utils.error(Utils.ADD_ON_DOES_NOT_EXISTS))
                } else {
                    res.status(200).json(Utils.success(null, Utils.ADD_ON_DELETED))
                }
            })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

/*
    *method to get list of add ons
    *searching 
    *sorting
    *pagination
*/

exports.get_add_ons = function (req, res) {
    const limit = req.body.limit
    const offset = req.body.offset
    const skip = limit * (offset - 1)
    let query = { isDeleted: false, isActive: true }
    let sortQuery = { createdAt: -1 }
    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }
    if (req.body.name && req.body.name != "") {
        query.name = { $regex: req.body.name, $options: 'i' }
    }
    AddOn.find(query).skip(skip).limit(limit).sort(sortQuery)
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response.length > 0) {
                AddOn.countDocuments(query).exec((err, count) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        res.status(200).json(Utils.successWithCount(response, Utils.DATA_FETCH, count))
                    }
                })
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

/*
    *method to edit add ons
*/

exports.edit_add_on = function (req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkName: function (callback) {
                AddOn.find({ isDeleted: false, name: req.body.name, _id: { $ne: req.body.addOnId } })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.ADD_ON_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            editAddOn: ['checkName', (results, callback) => {
                AddOn.findOneAndUpdate({ _id: req.body.addOnId, isDeleted: false }, { $set: req.body }).exec((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        if (result) {
                            callback(null, Utils.ADD_ON_UPDATED)
                        } else {
                            callback(Utils.ADD_ON_DOES_NOT_EXISTS)
                        }
                    }
                })
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.editAddOn))
            }
        })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

/*
    *method to get list of add ons
    *searching 
    *sorting
    *pagination
*/

exports.get_all_add_ons_and_stats = function (req, res) {
    let query = { isDeleted: false, isActive: true }
    async.auto({
        fetchUsertype:(callback)=>{
            UserType.find({isDeleted: false, isActive: true ,name: { $in: ['Contractor', 'Building Representative']}}).exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    callback(null, result)
                }
            })    
        },
        fetchPlanType:['fetchUsertype', (data,callback)=>{
            PlanType.find(query).populate('userType').exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    
                    response = {
                        userTypes: data.fetchUsertype,
                        planTypes: result
                    }
                    callback(null, response)
                }
            })
        }],
        fetchAddOns:['fetchPlanType',(data,callback)=>{
            AddOn.find(query).exec((err, result) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        response = {
                            userTypes: data.fetchPlanType.userTypes,
                            planTypes: data.fetchPlanType.planTypes,
                            addOns: result
                        }
                        callback(null, response)
                        // res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
                    }
                })
        }],
        fetchDuration: ['fetchAddOns', (data, callback) => {
            DurationType.find(query).exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    console.log(result)
                    response = {
                        userTypes: data.fetchAddOns.userTypes,
                        planTypes:  data.fetchAddOns.planTypes,
                        addOns:  data.fetchAddOns.addOns,
                        durations: result
                    }
                    res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
                }
            })
        }]
    })

}

// get  add on  by id

exports.get_add_on_by_id = function(req,res) {
    AddOn.findOne({ _id: req.params.addOnId, isDeleted: false, isActive: true })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else { (response) 
                res.status(200).json(Utils.success(response, Utils.DATA_FETCHED))
            }
        })
}




