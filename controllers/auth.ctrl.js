const jwt = require('jsonwebtoken');
const passport = require('passport');
const config = require('../utils/config');
const common = require('../common/common');
const Utils = require('../utils/response');
const User = require('../models/User');
const UserPlan = require('../models/UserPlan');
const async = require('async');
const bcrypt = require('bcryptjs');
const SubscriptionPlan = require('../models/SubscriptionPlan');
const UserType = require('../models/UserType');
var mongoose = require('mongoose');
const PlanHighLight = require('../models/PlanHighLight');
const PlanType = require('../models/PlanType');
const Cms = require('../models/Cms');
const stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
var clearbit = require('clearbit')('sk_5ecd344adc5e5fd45efc833d8deb0139');
const AddOn = require('../models/AddOn');
const UserPlanAddOn = require('../models/UserPlanAddOn');
const _ = require('lodash')

/*
   *method for login
*/
exports.login = function (req, res, next) {
    passport.authenticate('local', { session: false }, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json(Utils.authorisedError(Utils.INCORRECT_CREDENTIALS))
        }
        req.login(user, { session: false }, (err) => {
            if (err) {
                res.send(err);
            }
            const token = jwt.sign({ id: user._id }, config.secret, { expiresIn: '24h' });
            return res.status(200).json(Utils.successWithToken(user, Utils.DATA_FETCH, token));
        });
    })
        (req, res);
};

/*
   *method for forgot password
*/
exports.forgot_password = function (req, res) {
    async.auto({
        checkUserEmail: (callback) => {
            //  console.log(req.get('host'));
            User.findOne({ email: req.body.email, isDeleted: false, isActive: true })
                .exec((err, response) => {
                    if (err) {
                        callback(err)
                    } else if (response) {
                        const token = jwt.sign({ id: response._id }, config.secret);
                        User.findOneAndUpdate({ '_id': response._id }, { $set: { resetPasswordToken: token } }, { upsert: false, new: true })
                            .exec((err, updatedResponse) => {
                                if (err) {
                                    callback(err)
                                } else {
                                    callback(null, updatedResponse)
                                }
                            })
                    } else {
                        callback(Utils.NOT_FOUND)
                    }
                })
        },
        sendEmail: ['checkUserEmail', (data, callback) => {
            common.emailFunction('forgot', data.checkUserEmail, 'Forgot password', req).
                then((sendResponse) => {
                    callback(null, Utils.EMAIL_SENT)
                }).catch((error) => {
                    callback(error)
                })
        }]
    }, (err, result) => {
        if (err) {
            res.status(400).json(Utils.error(err))
        } else {
            res.status(200).json(Utils.success(null, result.sendEmail))
        }
    })
}
/*
   *method for register
*/
exports.registration = async (req, res) => {
    userModel = req.body;
    const Detail = {};

    try {
        // check email and  exists phone
        var checkUser = await User.findOne({
            $and: [
                {
                    $or: [
                        { "phone": userModel.phone },
                        { "email": userModel.email }
                    ]
                },
                {
                    "isDeleted": false
                }
            ]
        });
        if (checkUser) {
            // if email exists
            if (checkUser.email == userModel.email) {
                // check inactive plan exists for this email
                const inactiveplan = await UserPlan.findOne({ user: checkUser._id, isActive: false });
                if (inactiveplan) {
                    // if exists the update it with new plan data
                    const myPlan = {
                        subscriptionPlan: req.body.subscriptionPlan,
                        addOns: req.body.addOns,
                    }
                    const update = await UserPlan.findOneAndUpdate(
                        { user: req.body.userId },
                        { $set: myPlan },
                    );
                    return res.status(200).json(Utils.success(checkUser, Utils.ACCOUNT_CREATED))
                }
                return res.status(400).json(Utils.error(Utils.EMAIL_ALREADY_EXISTS));
            }
            // if phone exists
            return res.status(400).json(Utils.error(Utils.PHONE_ALREADY_EXISTS));
        }
        if (userModel.userTypeName == "Contractor") {
            Detail.companyName = userModel.companyName;
            userModel.companyDetails = Detail;
        } else {
            Detail.alarmContractor = userModel.alarmContractor;
            Detail.sprinklerContractor = userModel.sprinklerContractor;
            Detail.extinguisherContractor = userModel.extinguisherContractor;
            userModel.contractorDetails = Detail;
        }
        //console.log(userModel);
        userModel.password = bcrypt.hashSync(userModel.password, 10);

        // get super admin id from UserType
        // const AdminData = await UserType.findOne({ name: 'Super Admin' }, { _id: 1 });
        // const userAdminData= User.findOne({ _id: AdminData._id }, { _id: 1 });
        // userModel.addedBy = userAdminData._id;
        // console.log('111', AdminData);
        const user = await User(userModel)
        var response = await user.save();

        // update plan
        var userData = await User.findOne({ email: userModel.email }, { _id: 1 });
        const myPlan = {
            subscriptionPlan: userModel.subscriptionPlan,
            user: userData.id,
            addOns: userModel.addOns
        }
        const plans = await UserPlan(myPlan)
        const saved = await plans.save();
        res.status(200).json(Utils.success(userData, Utils.ACCOUNT_CREATED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}



/*
    *method for getting all subscription plans with highlights and plan type
*/

exports.get_subscription_plan_with_highlight = async (req, res) => {
    var resData = {};
    var hplanQuery = [
        {
            $lookup:
            {
                from: "plantypes",
                localField: "_id",
                foreignField: "userType",
                as: "planData"
            },
        },
    ];
    let matchquery = { isDeleted: false, isActive: true, name: { $in: ['Contractor', 'Building Representative'] } }

    var match = { $match: matchquery };
    hplanQuery.push(match);
    try {
        var subData = await UserType.aggregate(hplanQuery);
        const q = { pageTitle: { '$regex': '^home$', '$options': 'i' } }
        const homeData = await Cms.findOne(q);
        const addOns = await AddOn.find({ isDeleted: false, isActive: true });
        console.log(subData.homeData);
        for (let indexn = 0; indexn < subData.length; indexn++) {
            for (let index = 0; index < subData[indexn].planData.length; index++) {
                var query1 = { "userType": subData[indexn].planData[index].userType, "planType": subData[indexn].planData[index]._id, "isDeleted": false, "isActive": true }
                var query2 = { "planTypeId": subData[indexn].planData[index]._id, "isActive": true, "isDeleted": null }
                subData[indexn].planData[index].subscription = await SubscriptionPlan.find(query1).populate('addOns').populate('duration');
                subData[indexn].planData[index].highlights = await PlanHighLight.findOne(query2);
                //   subData[indexn].home = subData.homeData;
            }
        }
        resData.planData = subData;
        resData.homeData = homeData;
        resData.addOnsData = addOns;
        res.status(200).json(Utils.success(resData, Utils.DATA_FETCHED));
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}



/*
    *method to get user types and plan types
*/

exports.get_plan_subscription_by_id = async (req, res) => {
    console.log('---', req.params.addOns);
    let matchquery = {}
    let Alldata = {};
    if (req.body.planTypeId && req.body.planTypeId != "") {
        matchquery._id = mongoose.Types.ObjectId(req.body.planTypeId);
    }
    if (req.body.userTypeId && req.body.userTypeId != "") {
        matchquery.userType = mongoose.Types.ObjectId(req.body.userTypeId);
    }
    var hplanQuery = [{ $match: matchquery }];
    try {
        const addOns = await AddOn.find({
            "_id": {
                $in: req.body.addOns
            }
        }
        );

        var subData = await PlanType.aggregate(hplanQuery);
        for (let index = 0; index < subData.length; index++) {
            var query1 = { "userType": subData[index].userType, "planType": subData[index]._id, "isDeleted": false, "isActive": true }
            var query2 = { "planTypeId": subData[index]._id }
            subData[index].subscription = await SubscriptionPlan.find(query1).populate('addOns').populate('duration');
            subData[index].highlights = await PlanHighLight.findOne(query2);
        }
        subData = subData.length > 0 ? subData[0] : subData;
        Alldata['highlightData'] = subData;
        Alldata['addOnsData'] = addOns;
        res.status(200).json(Utils.success(Alldata, Utils.DATA_FETCHED));
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}


/*
    *method for create stripe membership
*/

exports.create_membership = function (req, res) {
    async.auto({
        userInfo: function (callback) {
            User.findOne({
                _id: req.body.userId,
            }).populate('userTypeId')
                .exec((err, response) => {
                    if (err) {
                        callback(err)
                    } else if (response == null) {
                        callback(Utils.INVALID_USER_ID)
                    } else {
                        callback(null, response)
                    }
                });
        },
        createCustomer: ['userInfo', (results, callback) => {
            //  req.body.addOns= ['5d7b893c0a0048571bd1b347'];
            //req.body.totalAddOnsAmount= 345;
            console.log('token', req.body);
            //return false;
            stripeAPI.customers.create({
                email: results.userInfo.email,
                source: req.body.token.id,
                address: {
                    line1: results.userInfo.address,
                    city: results.userInfo.city,
                    state: results.userInfo.state,
                    postal_code: results.userInfo.zipcode
                },
                phone: results.userInfo.phone,
                description: 'Customer for ' + results.userInfo.email,
                name: results.userInfo.firstName + ' ' + results.userInfo.lastName,
            },
                function (err, data) {
                    if (err) {
                        callback(err.raw.message)
                    }
                    //res.status(200).json(Utils.success(data))
                    callback(null, data)
                });
        },
        ],
        createSubscription: ['createCustomer', (results, callback) => {
            let customer = results.createCustomer.id;
            let plan = req.body.stripePlanId;//req.body.stripePlanId;
            let requested_data = {
                customer: customer,
                items: [
                    {
                        plan: plan,
                    },
                ],
            }
            //   console.log(requested_data);

            stripeAPI.subscriptions.create(requested_data,
                function (err, data) {
                    // console.log(data);

                    if (err) {
                        callback(err.raw.message)
                    }
                    callback(null, data)
                });
        }],
        updateUserPlan: ['createSubscription', (results, callback) => {
            console.log(results);
            UserPlan.findOneAndUpdate({ user: req.body.userId },
                { $set: { isActive: true, stripeSubscriptionPlanId: results.createSubscription.id, stripeCustomerId: results.createSubscription.customer, stripePlanId: results.createSubscription.plan.id, stripeProductId: results.createSubscription.plan.product } })
                .exec((err, response) => {
                    //  console.log(response);
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, response)
                    }
                });
        }],
        getAddOnsData: ['createSubscription', (results, callback) => {
            if (req.body.addOns.length <= 0) {
                response = {};
                req.body.totalAddOnsAmount = '';
                callback(null, response)
            } else {
                AddOn.find({
                    "_id": {
                        $in: req.body.addOns
                    }
                }).exec((err, response) => {
                    if (err) {
                        callback(err)
                    } else {
                        const total_amount = response.map(item => item.price).reduce((prev, next) => prev + next);
                        console.log('total', total_amount);
                        req.body.totalAddOnsAmount = total_amount;
                        response.total_amount = total_amount;
                        console.log('body', req.body);
                        //const total_amount =
                        callback(null, response)
                    }
                });
            }
        }],
        createChargeForAddOns: ['getAddOnsData', (results, callback) => {
            console.log(results.createCustomer, 'iii', results.createCustomer.default_source);
            if (req.body.addOns.length <= 0) {
                response = {};
                callback(null, response)
            } else {
                stripeAPI.charges.create({
                    amount: req.body.totalAddOnsAmount * 100,
                    currency: "usd",
                    customer: results.createCustomer.id,
                    source: results.createCustomer.default_source, // obtained with Stripe.js
                    description: "Charge for " + results.userInfo.email,
                }, function (err, response) {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, response)
                    }
                });
            }
        }
        ],
        saveAddOnsResopnseData: ['createChargeForAddOns', (results, callback) => {
            console.log(results);

            if (req.body.addOns.length <= 0) {
                response = {};
                callback(null, response)
            } else {
                AddOnModel = [];
                req.body.addOns.map(function (id) {
                    AddOnInsert = {};
                    AddOnInsert.user = req.body.userId;
                    AddOnInsert.plan = results.updateUserPlan._id;
                    AddOnInsert.transactionId = results.createChargeForAddOns.balance_transaction;
                    AddOnInsert.addOn = id;
                    AddOnModel.push(AddOnInsert);
                });
                console.log('addOns', AddOnModel);
                UserPlanAddOn.insertMany(AddOnModel, (err, response) => {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, response)
                    }
                });
            }
        }],

        emailSend: ['saveAddOnsResopnseData', (data, callback) => {
            let userInfo = data.userInfo;
            userInfo.planData = data.createSubscription.plan;
            userInfo.planData.amount = '$ ' + (userInfo.planData.amount / 100);
            userInfo.planData.addOnsAmount = req.body.totalAddOnsAmount;
            userInfo.template = 'subscriptionTemplate.ejs';
            common.commonEmailFunction('subscription_plan', userInfo, 'Subscription Plan', req).
                then((sendResponse) => {
                    callback(null, Utils.EMAIL_SENT)
                }).catch((error) => {
                    callback(error)
                })
        }]
    },
        (err, results) => {
            console.log(results);
            const token = jwt.sign({ id: req.body.userId }, config.secret, { expiresIn: '24h' });
            results.token  = token;
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(results, Utils.MENBERSHIP_CREATED))
            }
        })
};

/*
    *method for update user plan
*/

exports.updateUserPlan = async (req, res) => {
    const myPlan = {
        subscriptionPlan: req.body.subscriptionPlan,
        addOns: req.body.addOns
    }
    try {
        var response = await UserPlan.findOneAndUpdate(
            { user: req.body.userId },
            { $set: myPlan },
        );

        const updateUser = await User.findOneAndUpdate(
            { _id: req.body.userId },
            { $set: { userTypeId: req.body.userTypeId } }
        );
        res.status(200).json(Utils.success(response, Utils.USER_PLAN_UPDATED))
    } catch (error) {
        res.status(400).json(Utils.error(response))
    }
}


/*
    *method for send contact us mail
*/

exports.contactUs = function (req, res) {
    async.auto({
        userType: function (callback) {
            UserType.findOne({
                name: 'Super Admin',
            })
                .exec((err, response) => {
                    console.log('t1', response);
                    if (err) {
                        callback(err)
                    } else if (response == null) {
                        callback(Utils.INVALID_USER_ID)
                    } else {
                        callback(null, response)
                    }
                });
        },
        userInfo: ['userType', (data, callback) => {
            User.findOne({
                userTypeId: data.userType._id,
            })
                .exec((err, response) => {
                    console.log('t2', response);

                    if (err) {
                        callback(err)
                    } else if (response == null) {
                        callback(Utils.INVALID_USER_ID)
                    } else {
                        callback(null, response)
                    }
                });
        }],
        emailSend: ['userInfo', (data, callback) => {
            let userInfo = req.body;
            userInfo.formEmail = userInfo.email;
            userInfo.email = data.userInfo.email; // admin email
            userInfo.template = 'contactusTemplate.ejs';
            common.commonEmailFunction('contact_us', userInfo, 'Contact us', req).
                then((sendResponse) => {
                    callback(null, Utils.EMAIL_SENT)
                }).catch((error) => {
                    callback(error)
                })
        }]
    },
        (err, results) => {
            console.log(results);
            //return false;
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.emailSend))
            }
        })
};


// get page by title (frontend)
exports.get_page_by_title = function (req, res) {
    console.log(req.params.title);
    const search = { pageTitle: { '$regex': '^' + req.params.title + '$', '$options': 'i' } }
    Cms.findOne(search, { 'pageTitle': 1, "metaKeywords": 1, "metaDescription": 1, "pageContent": 1, "createdAt": 1, "parentPage": 1 }).populate('parentPage')
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCHED))
            } else {
                res.status(404).json(Utils.dataError(Utils.NO_DATA_FOUND))
            }
        })
}


// check email exists
exports.check_email_exists = async (req, res) => {
    try {
        var checkEmail = await User.findOne({ "email": req.params.email, "isDeleted": false }).count();
        if (checkEmail) {
            return res.status(200).json(Utils.success(checkEmail, Utils.DATA_FETCHED))
        }
        return res.status(400).json(Utils.error(Utils.EMAIL_ALREADY_EXISTS));
    } catch (error) {
        res.status(400).json(Utils.error(error.message))
    }
}

// get all cms pages by page type

exports.get_pages_by_page_type = function (req, res) {
    query = { isDeleted: false, isActive: true, pageType: req.params.pageType };
    Cms.find({ isDeleted: false, isActive: true, pageType: req.params.pageType }, { slug: 1, pageTitle: 1, _id: 1 })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response.length > 0) {
                Cms.countDocuments(query).exec((err, count) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        res.status(200).json(Utils.successWithCount(response, Utils.DATA_FETCH, count))
                    }
                })
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

// get  cms page by slug

exports.get_page_by_slug = function (req, res) {
    Cms.findOne({ 'slug': req.params.slug, isDeleted: false, isActive: true })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                (response)
                res.status(200).json(Utils.success(response, Utils.DATA_FETCHED))
            }
        })
}

/*
    *method for test clearbit
*/

exports.clearbit = async (req, res) => {
    try {
        // if(req.params.type=='company'){
        //     const data = await clearbit.Company.find({ domain: req.params.email, stream: true });
        // } else{
        //     const data = await clearbit.Person.find({email: req.params.email, stream: true});
        // }
        const data = await clearbit.Enrichment.find({ email: req.body.email, stream: true });
        var clearbitData = {
            'personalDetail': {
                'firstName': _.has(data, 'person.name.givenName') ? data.person.name.givenName : '',
                'lastName': _.has(data, 'person.name.familyName') ? data.person.name.familyName : ''
            },
            'companyDetail': {
                'title': _.has(data, 'person.employment.title')? data.person.employment.title : '',
                'department': _.has(data, 'company.category.sector') ? data.company.category.sector : '',
                'name': _.has(data, 'company.name') ? data.company.name : ''
            }
        }

        res.status(200).json(Utils.success(clearbitData, Utils.DATA_FETCHED))
    } catch (error) {
        res.status(400).json(Utils.error(error.message))
    }
}