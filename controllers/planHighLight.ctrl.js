const Utils = require('../utils/response');
const config = require('../utils/config');
const PlanHighLight = require('../models/PlanHighLight');
const PlanType = require('../models/PlanType');
const UserType = require('../models/UserType');
const async = require('async');
var mongoose = require('mongoose');
const stripe = require('../common/stripe');

/*
   *method for add highlight
*/

exports.add_highlight = async (req, res) => {
    if (req.user.userTypeId.name == "Super Admin") {
        highlightModel = req.body;
        highlightModel.planTypeId = highlightModel.planTypeId;
        highlightModel.userTypeId = highlightModel.userTypeId;
        let query = {
            planTypeId: mongoose.Types.ObjectId(highlightModel.planTypeId),
            isDeleted: null
        }
        // check plan highlight exists
        var count = await PlanHighLight.find(query).count();
        if (count) {
            return res.status(400).json(Utils.error(Utils.HIGHLIGHT_ALREADY_EXISTS))
        }
        const highlight = await PlanHighLight(highlightModel)
        var response = await highlight.save();
        if (response) {
            res.status(200).json(Utils.success(response, Utils.HIGHLIGHT_CREATED))
        } else {
            res.status(400).json(Utils.error(response))
        }
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

/*
    *method for deleting plan highlight
*/

exports.delete_highlight = async (req, res) => {
    if (req.user.userTypeId.name == "Super Admin") {
        try {
            var response = await PlanHighLight.findOneAndUpdate(
                { _id: req.params.highlightId },
                { $set: { isDeleted: true, DeletedAt: Date.now() } },
            );
            res.status(200).json(Utils.success(response, Utils.HIGHLIGHT_DELETED))
        } catch (error) {
            res.status(400).json(Utils.error(response))
        }
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

/*
    *method for getting list of highlight plans
    *search plan highlight by name
    *pagination
    *sorting
*/

exports.get_plans_highlight = async (req, res) => {
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const skip = limit * (offset - 1)
    let query = { isDeleted: null }
    let sortQuery = { createdAt: -1 }
    var match = { isDeleted: null }
    //{"planData.name": -1}
    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }
    if (req.body.planTypeId && req.body.planTypeId != "") {
        match.planTypeId = mongoose.Types.ObjectId(req.body.planTypeId);
        query.planTypeId = req.body.planTypeId;

    }
    if (req.body.userTypeId && req.body.userTypeId != "") {
        match.userTypeId = mongoose.Types.ObjectId(req.body.userTypeId);
        query.userTypeId = req.body.userTypeId;
    }
    var hplanQuery = [
        { $match: match }
        , {
            $lookup:
            {
                from: "plantypes",
                localField: "planTypeId",
                foreignField: "_id",
                as: "planData"
            },
        },
        {
            $lookup:
            {
                from: "usertypes",
                localField: "userTypeId",
                foreignField: "_id",
                as: "userData"
            },
        },
        { $sort: sortQuery },
        { $skip: skip },
        { $limit: limit },
        {
            "$project": {
                "userTypeId": 1,
                "planTypeId": 1,
                "planHighLights": 1,
                "description": 1,
                "isDeleted": 1,
                "userData": {
                    $arrayElemAt: ["$userData", 0]
                },
                "planData": {
                    $arrayElemAt: ["$planData", 0]
                }
            }
        }
    ];
    try {
        highlightData = await PlanHighLight.aggregate(hplanQuery);
        var count = await PlanHighLight.find(query).count();
        if (!count) {
            res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
        } else {
            res.status(200).json(Utils.successWithCount(highlightData, Utils.DATA_FETCH, count))
        }
    } catch (error) {
        res.status(400).json(Utils.error(error));
    }
}
/*
    *method to get highlightby id
*/
exports.get_highlightplan_by_id = function (req, res) {
    PlanHighLight.findOne({ _id: req.params.highlightId })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

/*
    *method for editing highlight plan
*/

exports.edit_highlightplan = async (req, res) => {
    if (req.user.userTypeId.name == "Super Admin") {
        var response = await PlanHighLight.findOneAndUpdate({ _id: req.body.highlightId }, { $set: req.body });
        if (response) {
            res.status(200).json(Utils.success(response, Utils.HIGHLIGHT_UPDATED))
        } else {
            res.status(400).json(Utils.error(response))
        }
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}


/*
    *method to get user types and plan types
*/

exports.get_all_user_plan_types = function (req, res) {
    let query = { isDeleted: false, isActive: true }
    async.auto({
        fetchUsertype: (callback) => {
            UserType.find({isDeleted: false, isActive: true ,name: { $in: ['Contractor', 'Building Representative']}}).exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    callback(null, result)
                }
            })
        },
        fetchPlanType: ['fetchUsertype', (data, callback) => {
            PlanType.find(query).populate('userType').exec((err, result) => {
                if (err) {
                    res.status(400).json(Utils.error(err))
                } else {
                    response = {
                        userTypes: data.fetchUsertype,
                        planTypes: result
                    }
                    res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
                }
            })
        }],
    })
}