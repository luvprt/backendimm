const Utils = require('../utils/response');
const config = require('../utils/config');
const Cms = require('../models/Cms');
const async = require('async');
//const slugify = require('@sindresorhus/slugify');


exports.add_cms_page = function(req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkPageTitle: function(callback) {
                Cms.find({ isDeleted: false, pageTitle:{'$regex' : '^'+req.body.pageTitle+'$', '$options' : 'i'} })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.PAGE_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            insertCmsPage: ['checkPageTitle', (results, callback) => {
                if (req.body.parentPage == undefined || req.body.parentPage == null || req.body.parentPage == '') {
                    req.body.parentPage = null;
                }
               // req.body.slug = slugify(req.body.pageTitle);
            	Cms(req.body).save((err,result)=>{
            		if (err) {
            			callback(err)
            		}else{
            			callback(null, Utils.PAGE_ADDED)
            		}
            	})
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.insertCmsPage))
            }
        })
    }else {
	    res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
	} 
};

exports.get_cms_pages = function(req,res) {
    const limit = parseInt(req.body.limit)
    const offset = parseInt(req.body.offset)
    const skip = limit * (offset - 1)
    let query = { isDeleted: false, isActive: true }
    let sortQuery = { createdAt: -1 }
    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }
    if (req.body.pageTitle && req.body.pageTitle != "") {
        query.pageTitle = { $regex: req.body.pageTitle, $options: 'i' }
    }

    if (req.body.pageType && req.body.pageType != "") {
        query.pageType = { $regex: req.body.pageType, $options: 'i' }
    }
    Cms.find(query).skip(skip).limit(limit).sort(sortQuery)
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response.length > 0) {
                Cms.countDocuments(query).exec((err, count) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        res.status(200).json(Utils.successWithCount(response, Utils.DATA_FETCH, count))
                    }
                })
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

exports.get_page_by_id = function(req,res) {
    Cms.findOne({ '_id': req.params.pageId, isDeleted: false, isActive: true })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCH))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

exports.edit_cms_page = function(req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        async.auto({
            checkPageTitle: function(callback) {
                Cms.find({ isDeleted: false, pageTitle:{'$regex' : '^'+req.body.pageTitle+'$', '$options' : 'i'}, _id: { $ne: req.body.pageId } })
                    .exec((err, response) => {
                        if (err)
                            callback(err)
                        else if (response.length > 0) {
                            callback(Utils.PAGE_EXISTS)
                        } else {
                            callback(null, null)
                        }
                    })
            },
            editCmsPage: ['checkPageTitle', (results, callback) => {
                // if parent page  is not comming
                if (req.body.parentPage == undefined || req.body.parentPage == null || req.body.parentPage == '') {
                    req.body.parentPage = null;
                }
                //req.body.slug = slugify(req.body.pageTitle);
                Cms.findOneAndUpdate({ _id: req.body.pageId, isDeleted: false }, { $set: req.body }).exec((err, result) => {
                    if (err) {
                        callback(err)
                    } else {
                        if (result) {
                            callback(null, Utils.PAGE_UPDATED)
                        } else {
                            callback(Utils.PAGE_DOES_NOT_EXISTS)
                        }
                    }
                })
            }]
        }, (err, results) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else {
                res.status(200).json(Utils.success(null, results.editCmsPage))
            }
        })
    } else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

exports.delete_cms_page = function(req, res) {
    if (req.user.userTypeId.name == "Super Admin") {
        Cms.findOneAndUpdate({ _id: req.params.pageId, isDeleted: false }, { $set: { isDeleted: true, deletedAT:Date.now() } })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response == null) {
                res.status(400).json(Utils.error(Utils.PAGE_DOES_NOT_EXISTS))
            } else {
                res.status(200).json(Utils.success(null, Utils.PAGE_DELETED))
            }
        })
    }else {
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
    }
}

// get all cms pages

exports.get_all_cms_pages = function(req,res) {
    query={isDeleted: false, isActive: true};
    Cms.find({isDeleted: false, isActive: true },{ pageTitle: 1, _id: 1 })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response.length > 0) {
                Cms.countDocuments(query).exec((err, count) => {
                    if (err) {
                        res.status(400).json(Utils.error(err))
                    } else {
                        res.status(200).json(Utils.successWithCount(response, Utils.DATA_FETCH, count))
                    }
                })
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}



