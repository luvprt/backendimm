const Utils = require('../utils/response');
const config = require('../utils/config');
const BillingAndServiceLocation = require('../models/BillingAndServiceLocation');
const User = require('../models/User');
const async = require('async');
var mongoose = require('mongoose');
const UserType = require('../models/UserType');


/*
   *method for add location
*/ 

exports.add_location = async (req, res) => {
    locationModel = req.body;
    locationModel.addedBy = req.user.id;
    try {
        // check if address is already exists matched
        // const checkAddress = await BillingAndServiceLocation.findOne({isDeleted: false,user:locationModel.user,locationType:locationModel.locationType,postalCode:locationModel.postalCode,streetAddress:locationModel.streetAddress}).count();
        // if(checkAddress){
        //     return res.status(400).json(Utils.error(Utils.ADDRESS_ALREADY_EXISTS_FOR_THIS_USER));
        // } 
        const location = await BillingAndServiceLocation(locationModel)
        var response = await location.save();
        res.status(200).json(Utils.success(response, Utils.LOCATION_CREATED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}


/*
    *method for delete location
*/

exports.delete_location = async (req, res) => {
        try {
            var response = await BillingAndServiceLocation.findOneAndUpdate(
                { _id: req.params.locationId },
                { $set: { isDeleted: true, deletedAt: Date.now() } },
            );
            res.status(200).json(Utils.success(response, Utils.LOCATION_DELETED))
        } catch (error) {
            res.status(400).json(Utils.error(error))
        }
}

/*
    *method for getting list of location
    *search location by name
    *pagination
    *sorting
*/

exports.get_locations = async (req, res) => {
    const limit = parseInt(req.body.limit);
    const offset = parseInt(req.body.offset);
    const skip = limit * (offset - 1)
    let sortQuery = { createdAt: -1 }

    // if contracter is online, only visble contracter added loactions
    if(req.user.userTypeId.name=='Contractor'){
        var match = { isDeleted: false,addedBy: req.user._id }
        var query = { isDeleted: false,addedBy: req.user._id }
    } else{
        var query = { isDeleted: false }
        var match = { isDeleted: false }
    }

    if (req.body.sortingField && req.body.sortingOrder) {
        sortQuery = {
            [req.body.sortingField]: req.body.sortingOrder
        }
    }

    // search location
    if (req.body.search && req.body.search) {
        match.streetAddress = {'$regex' : '^'+req.body.search, '$options' : 'i'};
        query.streetAddress =  {'$regex' : '^'+req.body.search, '$options' : 'i'};
    }

    if (req.body.locationType && req.body.locationType) {
        match.locationType = req.body.locationType;
        query.locationType = req.body.locationType;
    }

    var mainQuery = [
            { $match: match },
            {
            $lookup:
            {
                from: "users",
                localField: "user",
                foreignField: "_id",
                as: "user"
            },
        },
        {
            $lookup:
            {
                from: "users",
                localField: "addedBy",
                foreignField: "_id",
                as: "addedBy"
            },
        },
        { $sort: sortQuery },
        { $skip: skip },
        { $limit: limit },
        {
            "$project": {
                "locationType":1 ,
                "billToAlias": 1,
                "buildingAlias": 1,
                "streetAddress": 1,
                "apt": 1,
                "city": 1,
                "state": 1,
                "postalCode": 1,
                "isDeleted": 1,
                "isActive": 1,
                "addedBy": {
                    $arrayElemAt: ["$addedBy", 0]
                },
                "user": {
                    $arrayElemAt: ["$user", 0]
                }
            }
        }
    ];
   try {
        const locationData = await BillingAndServiceLocation.aggregate(mainQuery);
        var count = await BillingAndServiceLocation.find(query).count();
            res.status(200).json(Utils.successWithCount(locationData, Utils.DATA_FETCH, count))
    } catch (error) {
        res.status(400).json(Utils.error(error.message));
    }
}
/*
    *method to get location by location id
*/
exports.get_location_by_id = function (req, res) {
    BillingAndServiceLocation.findOne({ _id: req.params.locationId })
        .exec((err, response) => {
            if (err) {
                res.status(400).json(Utils.error(err))
            } else if (response) {
                res.status(200).json(Utils.success(response, Utils.DATA_FETCHED))
            } else {
                res.status(404).json(Utils.dataError(null, Utils.NO_DATA_FOUND))
            }
        })
}

/*
    *method to get customers by location
*/
exports.get_customers_by_location = async  (req, res) =>{
    try {
        const  query= { isDeleted: false,streetAddress: req.body.location,locationType: req.body.locationType }

           // only visble contracter added loactions
        if(req.user.userTypeId.name=='Contractor'){
            query.addedBy= req.user._id;
        } 
        var customers = await BillingAndServiceLocation.find(query,{user:1,streetAddress:1,locationType:1}).populate('user', { firstName:1,lastName:1,email:1,phone:1 });
        
        //  var user_ids = ids.map(function(id) { return id.user; });
       // var customers = await User.find({_id:{ $in: user_ids}},{firstName:1,lastName:1,email:1,phone:1});
        res.status(200).json(Utils.success(customers, Utils.DATA_FETCHED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}



/*
    *method to get customers by role
*/
exports.get_customers_by_role = async  (req, res) =>{
    try {
        var userType = await UserType.findOne({ name:"Building Representative"},{name:1});
        if(req.user.userTypeId.name=='Contractor'){
            var customers = await User.find({ addedBy: req.user._id, isDeleted: false,isActive: true},{firstName:1,lastName:1,email:1,phone:1,userTypeId:1});
        } else{
            var customers = await User.find(
                {
                    userTypeId: userType._id
                }
            ,{firstName:1,lastName:1,email:1,phone:1,userTypeId:1});
        }
        res.status(200).json(Utils.success(customers, Utils.DATA_FETCHED))
    } catch (error) {
        res.status(400).json(Utils.error(error.message))
    }
}



/*
    *method for editing location
*/

exports.edit_location = async (req, res) => {
    try {
        var response = await BillingAndServiceLocation.findOneAndUpdate({ _id: req.body.locationId }, { $set: req.body });
        res.status(200).json(Utils.success(response, Utils.LOCATION_UPDATED))
    } catch (error) {
        res.status(400).json(Utils.error(error));
    }
}



/*
   *method for save customer locations
*/

exports.save_customer_locations = async (req, res) => {
    locationModel = req.body.addresses;
    try {
        var response = await BillingAndServiceLocation.findOneAndUpdate({ _id: req.body.locationId }, { $set: req.body });
        res.status(200).json(Utils.success(location, Utils.LOCATION_CREATED))
    } catch (error) {
        res.status(400).json(Utils.error(error))
    }
}

/*
   *method for change location status
*/

exports.change_status = async (req, res) => {
    var msg = Utils.LOCATION_ACTIVE
    //req.body.status = req.body.status==1?false:true;
    if(req.body.status==false){
        msg = Utils.LOCATION_INACTIVE
    }
    try {
        var response = await BillingAndServiceLocation.findOneAndUpdate({ _id: req.body.locationId }, { $set: {isActive:req.body.status} });
        res.status(200).json(Utils.success(response,msg))
    } catch (error) {
        res.status(400).json(Utils.error(error.message))
    }
}




