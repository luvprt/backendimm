const bcrypt = require('bcryptjs');
const User = require('../models/User');
const config = require('../utils/config');
const nodemailer = require("nodemailer");
const ejs = require('ejs');

var stripe = module.exports = {
     createPlan(request) {
        var stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
        // request = {
        //     amount: 5000,
        //     interval: "month",
        //     product: {
        //       name: "silver special test"
        //     },
        //     currency: "usd",
        //   }
           stripeAPI.plans.create(request, 
            function(err, data) {
                if(err){
                    callback(err)
                   }
                callback(data, Utils.PLAN_ADDED)
            });
    },

	listPlans() {
        var stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
        stripeAPI.plans.list(
        //  { limit: 10 },
            function(err, data) {
                if(err){
                    return err;
                }
                return data;
        });
	},

	generateToken(request) {
        var stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
        request = {
            card: {
              number: '4242424242424242',
             exp_month: 12,
             exp_year: 2020,
              cvc: '123'
            }
          }
        stripeAPI.tokens.create(request,
            function(err, data) {
                if(err){
                   // JSON.parse(data);
                 //   return res.status(200).json(Utils.success(err, Utils.HIGHLIGHT_DELETED));
                  }
                //  JSON.parse(data);
                //  res.status(200).json(Utils.success(data, Utils.HIGHLIGHT_DELETED))
        });
    },

	createCustomer(email="test@yopmail.com",source="tok_1F9nK42eZvKYlo2CZrLNrnJ4") {
        var stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
             stripeAPI.customers.create({
                 email: email,
                 source: source,
             },
                 function(err, data) {
                    if(err){
                        return err;
                    }
                    return data;
            });
    },

	createSubscription(customer="cus_Ff70eaNsAN9cKA",plan="plan_Ff6sDNG2C5duzP") {

        var stripeAPI = require("stripe")("sk_test_uEHQejO2RM5evEogyZBeG1ry00YEbP26hm");
        return stripeAPI.subscriptions.create({
            customer: customer,
            items: [
                {
                plan: plan,
                },
            ]
            // expand: ['latest_invoice.payment_intent'],
            },function(err, data) {
            if(err){
                return err;
            }
            return data;
        });
    }
}