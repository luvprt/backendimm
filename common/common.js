const bcrypt = require('bcryptjs');
const User = require('../models/User');
const UserType = require('../models/UserType');
const config = require('../utils/config');
const nodemailer = require("nodemailer");
const ejs = require('ejs');

var common = module.exports = {

    insertAdmin() {
        return new Promise(function(resolve, reject){
            UserType.findOne({name:'Super Admin', isActive:true, isDeleted:false}).exec((err, response)=>{
                if(err){
                    reject(err)
                } else if (response){
                    resolve(common.createSuperAdmin(response._id))
                } else{
                    const data = {
                        name: 'Super Admin'
                    }
                    UserType(data).save((error, result) => {
                        if (error) {
                            reject(error)
                        } else {
                            resolve(common.createSuperAdmin(result._id))
                        }
                    })
                }
            })
        })
    },

    createSuperAdmin(userTypeId){
        return new Promise(function(resolve, reject) {
            User.findOne({ email: config.adminEmail, userTypeId: userTypeId, isDeleted: false })
            .exec((err, response) => {
                if (err) {
                    reject(err)
                } else if (response) {
                    resolve(response)
                } else {
                    bcrypt.hash(config.adminPassword, 10, function(err, hashpassword) {
                        if (err) {
                            reject(err)
                        } else {
                            const data = {
                                email: config.adminEmail,
                                password: hashpassword,
                                firstName: 'super',
                                lastName: 'admin',
                                userTypeId: userTypeId
                            }
                            User(data).save((error, result) => {
                                if (error) {
                                    reject(error)
                                } else {
                                    resolve(result)
                                }
                            })
                        }
                    })
                }
            })
        })
    },

	emailFunction(type, response , textObj,req='') {
        if(req==''){
            var emailUrl= config.emailFullUrl;
        } else{
            var emailUrl = req.get('origin');
        }
        var token = response.resetPasswordToken;
        var record = {
            data: response,
            url: `${emailUrl}/auth/reset-password?token=${token}`,
            type
        }
        return new Promise(function(resolve, reject) {
            var path = require("path").join(__dirname, '..', 'templates', 'emailTemplate.ejs');
            ejs.renderFile(path, record).then((data) => {
                var mailOptions = {
                    from: config.adminEmail,
                    to: response.email,
                    subject: textObj,
                    html: data,
                };
                resolve(common.mailSend(mailOptions))
            }).catch((err) => {
                reject(err)
            }) 
        })
    },

    commonEmailFunction(type, response , textObj,req) {
        var record = {
            data: response,
            type
        }
        return new Promise(function(resolve, reject) {
            var path = require("path").join(__dirname, '..', 'templates', response.template);
            ejs.renderFile(path, record).then((data) => {
                if(response.formEmail){
                    var formEmail= response.formEmail;
                } else{
                    var formEmail= config.adminEmail;
                }
                var mailOptions = {
                    from: formEmail,
                    to: response.email,
                    subject: textObj,
                    html: data,
                };
                resolve(common.mailSend(mailOptions))
            }).catch((err) => {
                reject(err)
            }) 
        })
	},

	mailSend(mailOptions) {
        return new Promise(function(resolve, reject) {
            //create reusable transporter object using the default SMTP transport
	        let transporter = nodemailer.createTransport({
                service: 'gmail',
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
				auth: {
				    user: config.adminEmail,
				    pass: 'test@1234'
				}
            });
            

            transporter.sendMail(mailOptions).then((mailResponse)=>{
            	console.log('success')
            	resolve('success');
            }).catch((err)=>{
            	reject(err)
            });
        })
    },

    clearBit(request) {  
        
        return new Promise(function(resolve, reject) {
            var clearbit = require('clearbit')('sk_5ecd344adc5e5fd45efc833d8deb0139');
            clearbit.Person.find({email: 'alex@clearbit.com', stream: true})
            .exec((err, response) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(response);
                    resolve(response)
                }
            })
        })
    }

}
