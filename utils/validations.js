const { check, validationResult } = require("express-validator");

var validate = {

  /*
   * login validation
  */
 contractorRegisterValidate: () => {
    let response = [
      check("email").isEmail()
      .withMessage("Enter Valid email"),
      check('subscriptionPlan').exists().withMessage("subscription plan Id is required"),
      check('firstName').exists().withMessage("first name is required"),
      check('lastName').exists().withMessage("last name is required"),
      check("password").isLength({ min: 5 })
      .withMessage("Password must be at least 5 chars long "),
      check('phone').exists().withMessage("phone is required"),
      check('phone').isNumeric().withMessage("phone must be a number"),
      check('phone').isMobilePhone().withMessage("Please enter valid phone number"),
      check('address').exists().withMessage("address is required"),
      check('apt').exists().withMessage("apt is required"),
      check('city').exists().withMessage("city is required"),
      check('state').exists().withMessage("state is required"),
      check('zipcode').exists().withMessage("zipcode is required"),
      check('userTypeId').exists().withMessage("user type id is required"),
      check('addOns').exists().withMessage("addOns is required"),
  ];
    return response;
  }
  
}

module.exports = validate