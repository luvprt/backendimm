const Utils = require('../utils/response');

var middleware = {
  /*
// custom middleware for permorming action(only Admin and Contractor can perform action in customers)
  */
  PermissionMiddleware: (req,res,next) => {
      if (req.user.userTypeId.name == "Super Admin" || req.user.userTypeId.name=="Contractor") {
        next();
      } else{
        res.status(400).json(Utils.error(Utils.INVALID_USER_TYPE))
      }
  }
  
}

module.exports = middleware