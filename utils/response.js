var message = {

  INCORRECT_PASSWORD: 'Incorrect password',
  LOGIN_SUCCESS: 'Login successfully',
  NOT_FOUND: 'Email does not exist for any account',
  WORKING_SUCCESS: 'Working succesfully',
  EMAIL_SENT: 'Email sent successfully',
  INVALID_USER_TYPE: 'Invalid user type to perform the specific operation',
  NO_RECORD: 'No record found',
  DATA_FETCHED:'Data fetched successfully',
  VERIFIED:'Token verified successfully',
  EXPIRE_TOKEN_LINK: 'Reset password token expire',

 
  PAGE_ADDED: 'cms page added successfully',
  PAGE_EXISTS: 'cms page with entered title already exists',
  PAGE_UPDATED: 'cms page data successfully updated',
  PAGE_DOES_NOT_EXISTS: "cms page data doesn't exist for corresponding page id",
  PAGE_DELETED: "cms page successfully deleted",

  PLAN_ADDED: 'Subscription plan added successfully',
  PLAN_EXISTS: 'Subscription plan with this plan name already exists',
  PLAN_DOES_NOT_EXISTS: "Subscription plan doesn't exist for corresponding plan id",
  PLAN_DELETED: 'Subscription plan successfully deleted',
  PLAN_UPDATED: "Subscription plan successfully updated",

  ADD_ON_EXISTS: "Add on with this name already exists",
  ADD_ON_ADDED: "Add on added successfully",
  ADD_ON_DOES_NOT_EXISTS: "Add on doesn't exist for corresponding addon id",
  ADD_ON_DELETED: "Add on successfully deleted",
  ADD_ON_UPDATED: "Add on successfully updated",

   // by ns
  EMAIL_ALREADY_EXISTS: 'Email already exists',
  ACCOUNT_CREATED: "Account created successfully",
  INVALID_LOGO_FORMAT : "Invalid image format",
  LOGO_REQUIRED : "Logo is required",
  PHONE_ALREADY_EXISTS : "Phone number already exists",
  PASSWORD_NOT_MATCHED:'Password and confirm passowrd does not match',
  HIGHLIGHT_DELETED:'Plan highlight deleted successfully',
  HIGHLIGHT_UPDATED:'Plan highlight updated successfully',
  NO_DATA_FOUND:'No record found',
  HIGHLIGHT_ALREADY_EXISTS:"Highlight already exists for this plan",
  HIGHLIGHT_CREATED:"Plan highlight added successfully",
  REQUIRED_NUMBER_OF_PERSON:"Number of persons are required",
  MENBERSHIP_CREATED:"Membership subscription plan created successfully",
  INVALID_USER_ID : "Invalid user id",
  USER_PLAN_UPDATED:"User plan updated successfully",
  INCORRECT_CREDENTIALS:"Incorrect email or password",

  // LOCATION
  LOCATION_CREATED:"Location created successfully",
  LOCATION_DELETED:"Location deleted successfully",
  LOCATION_UPDATED:"Location updated sucessfully",
  LOCATION_INACTIVE:"Location deactivated sucessfully",
  LOCATION_ACTIVE:"Location activated sucessfully",
  ADDRESS_ALREADY_EXISTS_FOR_THIS_USER:"Address already exists for that customer.",

  // CUSTOMER
  ADD_CUSTOMER: "Customer added successfully.",

  // EMPLOYEE
  DATA_SAVED:"Data saved successfully",
  EMPLOYEE_DELETED:"Employee deleted successfully",
  EMPLOYEE_UPDATED:"Employee updated sucessfully",
  INVALID_IMAGE_FORMAT:'Invalid image format',
  PROFILE_IMAGE_REQUIRED:"Profile image is required",
  


  //DOCUMENT

  DOCUMENT_SAVED:"Docuemnt saved successfully",
  /*
   * success response with token
  */
  successWithToken: (data, message, token) => {
    let response = {
      statusCode: 200,
      error: false,
      success: true,
      message: message,
      data: data,
      token
    };
    return response;
  },

  /*
   * success response with count
   */
  successWithCount: (data, message, count) => {
    let response = {
      statusCode: 200,
      error: false,
      success: true,
      message: message,
      data: data,
      count
    };
    return response;
  },

  /*
   * success response without token
  */
  success: (data, message) => {
    let response = {
      statusCode: 200,
      success: true,
      message: message,
      data: data,
    };
    return response;
  },

  /*
   * data error response
  */
  error: (err) => {
    let response = {
      statusCode: 400,
      success: false,
      message: err,
      data: null
    };
    return response;
  },
  /*
   * data validation error response
   */
  sendError: (err) => {
    let response = {
        statusCode: 412,
        message: err,
        errorType: "VALIDATION_ERROR",
        data: null,
        success: false
    };
    return response;
  },

  /*
   * authorization error response
  */
  authorisedError: (err) => {
    let response = {
      statusCode: 401,
      success: false,
      message: err,
      data: null
    };
    return response;
  },
  
  /*
   * no data found error response
  */
  dataError: (err) => {
    let response = {
      statusCode: 404,
      success: false,
      message: err,
      data: []
    };
    return response;
  },
}
module.exports = message